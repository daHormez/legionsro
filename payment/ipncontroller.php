<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 01.05.2015
 * @time: 23:50
 * @copyright: 2015 solutionDrive GmbH
 */

// lol
$amountUsd = array('3.00','6.00','9.00','12.00','15.00','25.00');           // Edit The Price Here ! (must be double (XX.XX)!) .
// loler
$usdToSilks = array(3=>250, 6=>500, 9=>800, 12=>1125, 15=>1350, 25=>2500);  // Edit Silks From Price 10=>500 [10$ = 500 Silk] .

// the config
require_once("ConfigController.php");
$oConfig = ConfigController::getInstance();
//var_dump($oConfig);

// the database
require_once("../application/controllers/system/DatabaseAccountController.php");
$oDbAccount = DatabaseAccountController::getInstance();
//var_dump($oDbAccount);

// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 0);
// Set to 0 once you're ready to go live
define("USE_SANDBOX", 0);
define("LOG_FILE", "./ipn.log");
// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
    if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }

    $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);  // IPN fix

    $req .= "&$key=$value";
}
// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data
if(USE_SANDBOX == true) {
    $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
    $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}
$ch = curl_init($paypal_url);
if ($ch == FALSE) {
    return FALSE;
}
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
//curl.cainfo=<path-to>cacert.pem
// would be better in php.ini set it after setup to 1! - hormez
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: legion-sro'));
$cert = __DIR__ . "./cacert.pem";
curl_setopt($ch, CURLOPT_CAINFO, $cert);
$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
{
    if(DEBUG == true) {
        error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message. " . PHP_EOL, 3, LOG_FILE);
    }
    curl_close($ch);
    exit;
} else {
    // Log the entire HTTP response if debug is switched on.
    if(DEBUG == true) {
        error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
        error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
    }
    curl_close($ch);
}
// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));

error_log("STOPPED HERE !!!" . PHP_EOL, 3, LOG_FILE);

if (strcmp ($res, "VERIFIED") == 0 || preg_match("!(VERIFIED)\s*\Z!", $res)) {
    error_log("STILL VERIFIED !!!" . PHP_EOL, 3, LOG_FILE);
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment and mark item as paid.
    // assign posted variables to local variables
    //$item_name = $_POST['item_name'];
    //$item_number = $_POST['item_number'];
    $payment_status     = $_POST['payment_status'];
    $payment_amount     = $_POST['mc_gross'];
    $payment_currency   = $_POST['mc_currency'];
    $txn_id             = $_POST['txn_id'];
    $receiver_email     = $_POST['receiver_email'];
    $payer_email        = $_POST['payer_email'];
    $username           = $_POST['custom'];

    if ($payment_status != 'Completed') {
        // payment not completed
        error_log("payment canceled".PHP_EOL, 3, LOG_FILE);
    }

    if (!in_array($payment_amount, $amountUsd)) {
        // payment amount not correct
        error_log("paid not enough".PHP_EOL, 3, LOG_FILE);
    }

    if($payment_currency != 'USD'){
        // wrong currency
        error_log("wrong currency".PHP_EOL, 3, LOG_FILE);
    }

    $timenow = date("y-m-d H:i:s", time());

    $sSql = "INSERT INTO paypal (txn_id, payer_email, mc_gross, username, date) VALUES ('{$txn_id}', '{$payer_email}', {$payment_amount}, '{$username}', '{$timenow}')";
    $oStm = $oDbAccount->prepare($sSql);
    $blExec = $oStm->execute();

    if (!$blExec) {
        error_log("No new entry in Paypal Table -- SRO_VT_ACCOUNT --". PHP_EOL, 3, LOG_FILE);
    } else {
        error_log($oConfig->getVar("paypal_email").' - Paypal table new entry! In table paypal is a new entry txn_id = '.$txn_id.' payer_email = '.$payer_email. PHP_EOL, 3, LOG_FILE);
    }

    $silkAmount = $usdToSilks[(int)$payment_amount];

    $sSql = "exec CGI.CGI_WebPurchaseSilk '$txn_id', '$username', 0, $silkAmount, 0"; // will execute automated in game update of silks.
    $oStm = $oDbAccount->prepare($sSql);
    $blExec = $oStm->execute();


    if (!$blExec) {
        error_log("No silks added to account -- SRO_VT_ACCOUNT --". PHP_EOL, 3, LOG_FILE);
    } else {
        error_log($oConfig->getVar("paypal_email").'Silk gained -- User with E-Mail: payer_email = '.$payer_email. PHP_EOL, 3, LOG_FILE);
    }
}

if(DEBUG == true) {
    error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
} else if (strcmp ($res, "INVALID") == 0) {
    // log for manual investigation
    // Add business logic here which deals with invalid IPN messages
    if(DEBUG == true) {
        error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
    }
}