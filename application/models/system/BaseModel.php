<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 28.01.2015
 * @time: 08:39
 * @copyright: 2015 solutionDrive GmbH
 */
class BaseModel
{
    protected $_sDbTablename;
    protected $_oDbAccount;
    protected $_oDbShard;

    private $_aData; // holds all the data which loaded from a table by name

    /**
     * Base Model Constuctor
     *
     * @param string $sDbTablename A Tablename
     */
    public function __construct($sDbTablename)
    {
        $this->_sDbTablename  = $sDbTablename;
        /**
         * @var PDO $this->_oDbAccount
         */
        $this->_oDbAccount    = DatabaseAccountController::getInstance();
        $this->_oDbShard      = DatabaseShardController::getInstance();

        $this->checkIfTableExists($sDbTablename);
    }

    /**
     * Checks if table exists
     *
     * @param string $sDbTablename A Tablename
     *
     * @throws ClassfileException
     */
    public function checkIfTableExists($sDbTablename)
    {
        $oStm = $this->_oDbAccount->prepare("SELECT * FROM sys.Tables where name like '{$sDbTablename}'");
        $blExec = $oStm->execute();
        if (!$blExec) {
            $oStm1 = $this->_oDbShard->prepare("SELECT * FROM sys.Tables where name like '{$sDbTablename}'");
            $blExec = $oStm1->execute();

            if (!$blExec) {
                throw new ClassfileException("Tabelle '$sDbTablename' ist in der DB nicht vorhanden!");
            }
        }
    }

    public function getFieldData($sFieldname) {
        $sSql = "SELECT {$sFieldname} FROM {$this->_sDbTablename}";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $blExec = $oStm->execute();

        if(!$blExec) {
            $oStm1 = $this->_oDbShard->prepare($sSql);
            $blExec = $oStm1->execute();
        }

        if($blExec) {
            return $oStm->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    /**
     * Loads all values and columns as a key in an array
     *
     * @return array
     */
    public function loadAll()
    {
        $sSql = "SELECT * FROM {$this->_sDbTablename}";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $blExec = $oStm->execute();
        if(!$blExec) {
            $oStm = $this->_oDbShard->prepare($sSql);
            $blExec = $oStm->execute();
        }

        if($blExec) {
            $this->_aData = $oStm->fetchAll(PDO::FETCH_OBJ);
        }

        return $this->_aData;
    }

    /**
     * Gets the data of a table which was loaded
     *
     * @return mixed
     */
    public function getData()
    {
        $this->loadAll();
        return $this->_aData;
    }

    /**
     * Executes account sql
     *
     * @param $sSql
     *
     * @return mixed
     */
    public function accountExecute($sSql)
    {
        $oStm = $this->_oDbAccount->prepare($sSql);
        return $oStm->execute();
    }

    /**
     * Executes shard sql
     *
     * @param $sSql
     *
     * @return mixed
     */
    public function shardExecute($sSql)
    {
        $oStm = $this->_oDbShard->prepare($sSql);
        return $oStm->execute();
    }
}
