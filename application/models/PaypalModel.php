<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 25.04.2015
 * @time: 12:40
 * @copyright: 2015 solutionDrive GmbH
 */
class PaypalModel extends BaseModel
{
    protected $_sDbTablename = "paypal";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Checks if order dublicated
     *
     * @param $sId
     *
     * @return bool
     */
    public function isDublicated($sId)
    {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE txn_id = '{$sId}'";
        return $this->accountExecute($sSql);
    }

    /**
     * Registers order
     *
     * @param $sTransId
     * @param $sPayerMail
     * @param $sMcGross
     * @param $sUsername
     * @param $sTimeNow
     *
     * @return bool
     */
    public function registerOrder($sTransId, $sPayerMail, $sMcGross, $sUsername, $sTimeNow)
    {
        $sSql = "INSERT INTO {$this->_sDbTablename} (txn_id, payer_email, mc_gross, username, date) VALUES ('{$sTransId}', '{$sPayerMail}', {$sMcGross}, '{$sUsername}', '{$sTimeNow}')";
        return $this->accountExecute($sSql);
    }
}