<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class UserModel
{
    public $sUid = null;

    /**
     * Basic constructor
     *
     * @param string $sUid User Id
     */
    public function __construct($sUid)
    {
        $this->sUid = $sUid;
    }

    /**
     * Authenticates the User
     *
     * @param string $sLogin a username
     * @param string $sPassword a password
     *
     * @return string
     */
    public static function authenticateUser($sLogin, $sPassword)
    {
        $oUser = new DbUserModel();
        $oDbUser = $oUser->getUserByUsername($sLogin);

        if($oDbUser->StrUserID == $sLogin && $oDbUser->password == md5($sPassword)) {
            $sUid = $oDbUser->JID;
        } else {
            return 1;
        }

        return $sUid;
    }
}
