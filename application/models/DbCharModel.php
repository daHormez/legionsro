<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 23.04.2015
 * @time: 01:39
 * @copyright: 2015 solutionDrive GmbH
 */
class DbCharModel extends BaseModel
{
    protected $_sDbTablename = "_Char";

    /**
     * Char Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets user by id
     *
     * @param $sId
     *
     * @return mixed
     */
    public function getCharById($sId) {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE CharID = '{$sId}'";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Resets skill by name
     *
     * @param $sName
     */
    public function resetSkillByName($sName)
    {
        $sSql = "DECLARE @CharName varchar (64)
        DECLARE  @CharID int
        SET @CharName = '{$sName}' --Your Char Name
        SET @CharID = (SELECT CharID FROM _Char WHERE CharName16 = @CharName) --Do not change

            -- edit by InPanic Kev --
        update _ClientConfig
        set Data = '0'
        where CharID = @CharID
            -- edit by InPanic Kev --

        --- Do Not Change Here ---
        --- made by ModGift for RZ ---
        DELETE [dbo].[_CharSkill]
                                WHERE [CharID] = @CharID and [SkillID] > '2' and SkillID<'40'

        DELETE [dbo].[_CharSkill]
           WHERE [CharID] = @CharID and [SkillID] > '40' and SkillID<'70'

        DELETE [dbo].[_CharSkill]
           WHERE [CharID] = @CharID and [SkillID] > '70'

        UPDATE [dbo].[_CharSkillMastery]
           SET
              [Level] = '0'
         WHERE CharID= @CharID";

        $this->shardExecute($sSql);
    }

    /**
     * Resets skill by id
     *
     * @param $sId
     */
    public function resetSkillById($sId)
    {
        $sSql = "DECLARE @CharName varchar (64)
        DECLARE  @CharID int
        SET @CharIDSelect = '{$sId}' --Your Char id
        SET @CharID = (SELECT CharID FROM _Char WHERE CharID = @CharIDSelect) --Do not change

            -- edit by InPanic Kev --
        update _ClientConfig
        set Data = '0'
        where CharID = @CharID
            -- edit by InPanic Kev --

        --- Do Not Change Here ---
        --- made by ModGift for RZ ---
        DELETE [dbo].[_CharSkill]
                                WHERE [CharID] = @CharID and [SkillID] > '2' and SkillID<'40'

        DELETE [dbo].[_CharSkill]
           WHERE [CharID] = @CharID and [SkillID] > '40' and SkillID<'70'

        DELETE [dbo].[_CharSkill]
           WHERE [CharID] = @CharID and [SkillID] > '70'

        UPDATE [dbo].[_CharSkillMastery]
           SET
              [Level] = '0'
         WHERE CharID= @CharID";

        $this->shardExecute($sSql);
    }

    /**
     * Resets stat points
     *
     * @param $sId
     */
    public function resetStatById($sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET Strength = 20 + (CurLevel - 1), Intellect = 20 + (CurLevel - 1), RemainStatPoint = (CurLevel - 1) * 3 WHERE CharID = {$sId}";
        $this->shardExecute($sSql);
    }

    /**
     * Gets charnames
     *
     * @param $sId
     *
     * @return mixed
     */
    public function getCharnames($sId)
    {
        $sSql = "SELECT CharName16, NickName16 FROM {$this->_sDbTablename} WHERE CharID = '{$sId}'";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Gets the ranking chars
     *
     * @return mixed
     */
    public function getRankingChars()
    {
        $oAdminCharId = new DbUserModel();

        foreach($oAdminCharId->getAdminCharIdList() as $oChar) {
            $aIgnore[] = "CharID != '".$oChar->CharID."'";
        }

        if(!empty($aIgnore)) {
            $sIgnore = implode(" AND ", $aIgnore);
        } else {
            $sIgnore = "CharID != ''";
        }

        // Level Cap
        $oConfig = ConfigController::getInstance();
        $sLevelCap = $oConfig->getVar('level_cap');

        $sSql = "SELECT TOP 50 *, table_guild.Name as GuildName FROM {$this->_sDbTablename} as table_char, _Guild as table_guild WHERE {$sIgnore} AND table_char.CurLevel <= {$sLevelCap} AND table_char.GuildID = table_guild.ID ORDER BY table_char.CurLevel DESC, table_char.RemainGold DESC";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Gets the total char count
     *
     * @return mixed
     */
    public function getTotalCharCount()
    {
        $sSql = "SELECT COUNT(*) as TotalCharCount FROM {$this->_sDbTablename}";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @param $sLevel
     * @param $sId
     */
    public function setCurLevel($sLevel, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET CurLevel = '{$sLevel}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $sStrength
     * @param $sId
     */
    public function setStrength($sStrength, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET Strength = '{$sStrength}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $sIntellect
     * @param $sId
     */
    public function setIntellect($sIntellect, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET Intellect = '{$sIntellect}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iGold
     * @param $sId
     */
    public function setGold($iGold, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET RemainGold = '{$iGold}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iPoints
     * @param $sId
     */
    public function setSkillpoints($iPoints, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET RemainSkillPoint = '{$iPoints}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iPoints
     * @param $sId
     */
    public function setStatpoints($iPoints, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET RemainStatPoint = '{$iPoints}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iHp
     * @param $sId
     */
    public function setHp($iHp, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET HP = '{$iHp}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iMp
     * @param $sId
     */
    public function setMp($iMp, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET MP = '{$iMp}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iSize
     * @param $sId
     */
    public function setInventorySize($iSize, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET InventorySize = '{$iSize}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iLevel
     * @param $sId
     */
    public function setTraderLevel($iLevel, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET JobLvl_Trader = '{$iLevel}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iLevel
     * @param $sId
     */
    public function setHunterLevel($iLevel, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET JobLvl_Hunter = '{$iLevel}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iLevel
     * @param $sId
     */
    public function setRobberLevel($iLevel, $sId)
    {
        $sSql = "UPDATE {$this->_sDbTablename} SET JobLvl_Robber = '{$iLevel}' WHERE CharID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    /**
     * @param $iSilk
     * @param $sId
     */
    public function setSilk($iSilk, $sId)
    {
        $oDbSilkModel = new DbSilkModel();
        $oDbSilkModel->setSilkById($sId, $iSilk);
    }

    /**
     * @param $sId
     */
    public function setSilkEntry($sId)
    {
        $oDbSilkModel = new DbSilkModel();
        $oDbSilkModel->deleteSilkEntry($sId);
    }
}