<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 24.04.2015
 * @time: 16:02
 * @copyright: 2015 solutionDrive GmbH
 */
class AdminNewsModel extends BaseModel
{
    protected $_sDbTablename = "admin_news";

    /**
     * admin_news Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets all news
     *
     * @return array
     */
    public function getNews()
    {
        return $this->loadAll();
    }

    /**
     * Posts new news
     *
     * @param $sType
     * @param $sHeading
     * @param $sText
     */
    public function postNews($sType = "News", $sHeading, $sText)
    {
        $sDate = $this->_generateNewDate();
        $sSql = "INSERT INTO {$this->_sDbTablename} (HEADING, TEXT, TYPE, CREATE_DATE) VALUES ('{$sHeading}', '{$sText}', '{$sType}', '{$sDate}')";
        $this->shardExecute($sSql);
    }

    /**
     * Deletes news
     *
     * @param $sId
     */
    public function deleteNews($sId)
    {
        $sSql = "DELETE FROM {$this->_sDbTablename} WHERE ID = '{$sId}'";
        $this->shardExecute($sSql);
    }

    private function _generateNewDate()
    {
        $sTime = time();
        $sDate = date("Y-m-d H:i:s", $sTime);

        return $sDate;
    }
}