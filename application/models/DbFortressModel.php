<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 27.04.2015
 * @time: 16:42
 * @copyright: 2015 solutionDrive GmbH
 */

class DbFortressModel extends BaseModel
{

    protected $_sDbTablename = "_SiegeFortress";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets the fortress by id
     *
     * @param $sId
     *
     * @return object
     */
    public function getFortressById($sId)
    {
        $sSql = "SELECT TOP 1 * FROM {$this->_sDbTablename} WHERE FortressID = {$sId} LIMIT 1";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }
}