<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 27.04.2015
 * @time: 18:40
 * @copyright: 2015 solutionDrive GmbH
 */
class DbSilkModel extends BaseModel
{

    protected $_sDbTablename = "SK_Silk";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Deletes silk entry
     *
     * @param $sId
     */
    public function deleteSilkEntry($sId)
    {
        $oSilk = $this->getSilkById($sId);

        if($oSilk) {
            $sSql = "DELETE FROM {$this->_sDbTablename} WHERE JID = {$sId})";
            $this->accountExecute($sSql);
        }
    }

    /**
     * Sets the silk amount by id
     *
     * @param $sId
     * @param $iAmount
     */
    public function setSilkById($sId, $iAmount)
    {
        $oSilk = $this->getSilkById($sId);

        if(!$oSilk) {
            $sSql = "INSERT INTO {$this->_sDbTablename} (JID, silk_own) VALUES ({$sId}, {$iAmount})";
        } else {
            $sSql = "UPDATE {$this->_sDbTablename} SET silk_own = {$iAmount} WHERE JID = {$sId}";
        }

        $this->accountExecute($sSql);
    }

    /**
     * Gets the fortress by id
     *
     * @param $sId
     *
     * @return object
     */
    public function getSilkById($sId)
    {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE JID = {$sId}";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }
}