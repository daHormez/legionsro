<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 29.04.2015
 * @time: 16:27
 * @copyright: 2015 solutionDrive GmbH
 */
class DbSilkModel extends BaseModel
{

    protected $_sDbTablename = "lottery";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Sets up the lottery
     *
     * @throws ClassfileException
     */
    public function setupLottery()
    {
        $blTableExists = $this->checkIfTableExists($this->_sDbTablename);

        if (!$blTableExists) {
            $this->_setupDatabase();
        }
    }

    /**
     * Sets up the lottery database
     *
     * @throws ClassfileException
     */
    private function _setupDatabase()
    {

    }
}