<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 22.04.2015
 * @time: 18:09
 * @copyright: 2015 solutionDrive GmbH
 */

class DbUserModel extends BaseModel
{
    protected $_sDbTablename = "TB_User";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets user by username
     *
     * @param $sUsername
     *
     * @return mixed
     */
    public function getUserByUsername($sUsername) {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE StrUserID = '{$sUsername}'";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Gets user by email
     *
     * @param $sEmail
     *
     * @return mixed
     */
    public function getUserByEmail($sEmail) {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE Email = '{$sEmail}'";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Gets user by id
     *
     * @param $sId
     *
     * @return mixed
     */
    public function getUserById($sId) {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE JID = '{$sId}'";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Generates new User
     *
     * @param string $sUsername A username
     * @param string $sPassword A password
     * @param string $sEmail An email
     *
     * @return mixed
     */
    public function generateNewUser($sUsername, $sPassword, $sEmail)
    {
        $sSql = "INSERT INTO {$this->_sDbTablename} (StrUserID, password, Email) VALUES ('{$sUsername}', '{$sPassword}', '{$sEmail}')";
        $oStm = $this->_oDbAccount->prepare($sSql);

        return $oStm->execute();
    }

    /**
     * Changes the Email
     *
     * @param $sEmail
     * @param $sId
     */
    public function changeEmail($sEmail, $sId) {
        $sSql = "UPDATE {$this->_sDbTablename} SET Email = '{$sEmail}' WHERE JID = '{$sId}'";
        $this->accountExecute($sSql);
    }

    /**
     * Changes the Email
     *
     * @param $sMd5Password
     * @param $sId
     */
    public function changePassword($sMd5Password, $sId) {
        $sSql = "UPDATE {$this->_sDbTablename} SET Password = '{$sMd5Password}' WHERE JID = '{$sId}'";
        $this->accountExecute($sSql);
    }

    /**
     * Gets the total account count
     *
     * @return mixed
     */
    public function getTotalAccountCount()
    {
        $sSql = "SELECT COUNT(*) as TotalAccountCount FROM {$this->_sDbTablename}";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Gets a char list
     *
     * @param $sId
     */
    public function getCharIdList($sId)
    {
        $sSql = "SELECT * FROM _User WHERE UserJID = '{$sId}'";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Gets a char list
     *
     * @return array
     */
    public function getAdminCharIdList()
    {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE sec_primary = 1 AND sec_content = 1 OR sec_primary = 11 AND sec_content = 11";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        $oAdminUsers = $oStm->fetchAll(PDO::FETCH_OBJ);
        $aAdminChars = array();

        foreach($oAdminUsers as $oUser) {
            $aAdminChars = $this->getCharIdList($oUser->JID);
        }

        return $aAdminChars;
    }

    /**
     * Checks if user is an admin
     *
     * @param string $sId
     *
     * @return bool
     */
    public function isAdmin($sId)
    {
        $oUser = $this->getUserById($sId);

        if($oUser->sec_primary == '1' &&
           $oUser->sec_content == '1' ||
           $oUser->sec_primary == '11' &&
           $oUser->sec_content == '11') {
            return true;
        }

        false;
    }
}
