<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 27.04.2015
 * @time: 16:42
 * @copyright: 2015 solutionDrive GmbH
 */

class DbGuildModel extends BaseModel
{

    protected $_sDbTablename = "_Guild";

    /**
     * Guild Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets the fortress by id
     *
     * @param $sId
     *
     * @return object
     */
    public function getGuildById($sId)
    {
        $sSql = "SELECT * FROM {$this->_sDbTablename} WHERE ID = {$sId}";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Gets the top 50 guilds
     *
     * @return object
     */
    public function getAllGuilds()
    {
        $sSql = "SELECT TOP 50 * FROM {$this->_sDbTablename} ORDER BY Lvl DESC, GatheredSP DESC";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();
        $aGuilds = $oStm->fetchAll(PDO::FETCH_OBJ);

        $aAssignedGuilds = array();

        foreach($aGuilds as $oGuild) {
            $aCount = $this->getMemberCountByGuildId($oGuild->ID);
            $oGuild->MemberCount = $aCount['MemberCount'];

            $aAssignedGuilds[] = $oGuild;
        }

        return $aAssignedGuilds;
    }

    /**
     * Gets the guild member count by guild id
     *
     * @param $sId
     *
     * @return object
     */
    public function getMemberCountByGuildId($sId)
    {
        $sSql = "SELECT COUNT(*) as MemberCount FROM _GuildMember WHERE GuildID = {$sId}";
        $oStm = $this->_oDbShard->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_ASSOC);
    }
}