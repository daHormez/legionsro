<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 25.04.2015
 * @time: 08:58
 * @copyright: 2015 solutionDrive GmbH
 */
class DbCharJobModel extends BaseModel
{
    protected $_sDbTablename = "_CharTrijob";

    /**
     * Char job Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets the trader
     *
     * @return array
     */
    public function getAllTrader()
    {
        $sSql = "SELECT TOP 50 * FROM {$this->_sDbTablename} WHERE JobType = 1 ORDER BY Contribution desc, Level DESC, Exp DESC";
        $oTrader = $this->_oDbShard->prepare($sSql);
        $oTrader->execute();

        return $oTrader->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Gets the robber
     *
     * @return array
     */
    public function getAllRobber()
    {
        $sSql = "SELECT TOP 50 * FROM {$this->_sDbTablename} WHERE JobType = 2 ORDER BY Contribution desc,Level DESC, Exp DESC";
        $oTrader = $this->_oDbShard->prepare($sSql);
        $oTrader->execute();

        return $oTrader->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Gets the hunter
     *
     * @return array
     */
    public function getAllHunter()
    {
        $sSql = "SELECT TOP 50 * FROM {$this->_sDbTablename} WHERE JobType = 3 ORDER BY Contribution desc, Level DESC, Exp DESC";
        $oTrader = $this->_oDbShard->prepare($sSql);
        $oTrader->execute();

        return $oTrader->fetchAll(PDO::FETCH_OBJ);
    }
}