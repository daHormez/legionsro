<?php

/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 23.04.2015
 * @time: 01:39
 * @copyright: 2015 solutionDrive GmbH
 */
class DbCurrentUserModel extends BaseModel
{
    protected $_sDbTablename = "_ShardCurrentUser";

    /**
     * User Model construct
     */
    public function __construct()
    {
        parent::__construct($this->_sDbTablename);
    }

    /**
     * Gets user count!
     *
     * @return mixed
     */
    public function getCount() {
        $sSql = "SELECT top 1 * FROM {$this->_sDbTablename} WHERE nShardID = 64 ORDER BY nID desc";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $oStm->execute();

        return $oStm->fetch(PDO::FETCH_OBJ);
    }
}