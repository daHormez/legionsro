<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class DonateController extends BaseController
{

    /**
     * IndexAction
     */
    public function indexAction()
    {
        $this->_oView->assign('page', 'page/donate.tpl');
        $this->_oView->assign('sDonateActive', 'active');    // sets menu point to active state
        $this->_oView->display('layout/base.tpl');
    }

    /**
     * SuccessAction
     */
    public function successAction()
    {

    }
}
