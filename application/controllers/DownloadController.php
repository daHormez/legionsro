<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class DownloadController extends BaseController
{

    /**
     * IndexAction
     */
    public function indexAction()
    {
        $sDlOne   = $this->_oConfig->getVar("download_1");
        $sDlTwo   = $this->_oConfig->getVar("download_2");
        $sDlThree = $this->_oConfig->getVar("download_3");
        $sDlFour  = $this->_oConfig->getVar("download_4");

        $sDlOneLink   = $this->_oConfig->getVar("download_1_link");
        $sDlTwoLink   = $this->_oConfig->getVar("download_2_link");
        $sDlThreeLink = $this->_oConfig->getVar("download_3_link");
        $sDlFourLink  = $this->_oConfig->getVar("download_4_link");

        $this->_oView->assign('dlone', $sDlOne);
        $this->_oView->assign('dltwo', $sDlTwo);
        $this->_oView->assign('dlthree', $sDlThree);
        $this->_oView->assign('dlfour', $sDlFour);

        $this->_oView->assign('dlonelink', $sDlOneLink);
        $this->_oView->assign('dltwolink', $sDlTwoLink);
        $this->_oView->assign('dlthreelink', $sDlThreeLink);
        $this->_oView->assign('dlfourlink', $sDlFourLink);

        $this->_oView->assign('page', 'page/download.tpl');
        $this->_oView->assign('sDownloadActive', 'active');    // sets menu point to active state
        $this->_oView->display('layout/base.tpl');
    }
}
