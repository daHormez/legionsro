<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class AccountController extends BaseController
{

    private $_aPost = array();
    private $_aCharacterIdsList = array();

    /**
     * IndexAction
     */
    public function indexAction()
    {
        if($this->_oUser->isLoggedIn()) {
            // silk
            $oDbSilkModel = new DbSilkModel();
            $oSilk = $oDbSilkModel->getSilkById($this->_oUser->getUid());
            $this->_oView->assign("oSilk", $oSilk);

            // gold
            $this->_prepareCharList(); // prepares our character list
            $aInfos = $this->_getCharactersInfo();

            $this->_oView->assign('aInfos', $aInfos);

            // default
            $this->_oView->assign('page', 'page/account/main.tpl');
            $this->_oView->assign('sAccountActive', 'active');    // sets menu point to active state
            $this->_oView->display('layout/base.tpl');
        } else {
            $this->_oView->assign('error', "You have to login first");
            $this->_oView->display('error_login.tpl');
        }
    }

    /**
     * IndexAction
     */
    public function buysilkAction($aParams)
    {
        if($this->_oUser->isLoggedIn()) {
            if(isset($aParams['error'])) {
                $this->_oView->assign('error', 'Something goes wrong');
            }

            if(isset($aParams['success'])) {
                $this->_oView->assign('success', 'Coins added to your Account');
            }

            $this->_oView->assign('page', 'page/account/buysilk.tpl');
            $this->_oView->assign('sAccountActive', 'active');    // sets menu point to active state
            $this->_oView->display('layout/base.tpl');
        } else {
            $this->_oView->assign('error', "You have to login first");
            $this->_oView->display('error_login.tpl');
        }
    }

    /**
     * ChangepwAction
     */
    public function changepwAction($aParams)
    {
        if($this->_oUser->isLoggedIn()) {

            $this->_aPost = $_POST;

            $blAnyError = false;

            if ($this->_aPost['send'] == "change_password") {
                if ($this->_isDataComplete()) {
                    // check password repeat
                    if(!$this->_isPasswordMatch()) {
                        $this->_oView->assign("error", "Passwords don't match");
                        $blAnyError = true;
                    }

                    if (!$blAnyError) {
                        $this->_oDbUser->changePassword(md5($this->_aPost['change_password']), $this->_oUser->getUid());

                        $this->_oView->assign("success", "Password changed");
                    }
                } else {
                    $this->_oView->assign("error", "Fill all fields to complete password change");
                }
            }



            $this->_oView->assign('page', 'page/account/change_pw.tpl');
            $this->_oView->assign('sAccountActive', 'active');    // sets menu point to active state
            $this->_oView->display('layout/base.tpl');
        } else {
            $this->_oView->assign('error', "You have to login first");
            $this->_oView->display('error_login.tpl');
        }
    }

    /**
     * ChangeemailAction
     */
    public function changeemailAction($aParams)
    {
        if($this->_oUser->isLoggedIn()) {
            $this->_aPost = $_POST;

            $blAnyError = false;

            if ($this->_aPost['send'] == "change_email") {
                if ($this->_isDataComplete()) {
                    // check email already exists
                    if ($this->_isEmailTaken()) {
                        $this->_oView->assign("error", "E-Mail already exists");
                        $blAnyError = true;
                    } elseif ($this->_isEmailWellFormatted() <= 0) {
                        $this->_oView->assign("error", "Type correct E-Mail");
                        $blAnyError = true;
                    }

                    if (!$blAnyError) {
                        $this->_oDbUser->changeEmail($this->_aPost['change_email'], $this->_oUser->getUid());

                        $this->_oView->assign("sNewEmail", $this->_aPost['change_email']);
                        $this->_oView->assign("success", "E-Mail changed");
                    }
                } else {
                    $this->_oView->assign("error", "Fill all fields to complete E-Mail change");
                }
            }

            $this->_oView->assign('page', 'page/account/change_email.tpl');
            $this->_oView->assign('sAccountActive', 'active');    // sets menu point to active state
            $this->_oView->display('layout/base.tpl');
        } else {
            $this->_oView->assign('error', "You have to login first");
            $this->_oView->display('error_login.tpl');
        }
    }

    /**
     * ResetcharAction
     */
    public function resetcharAction($aParams)
    {
        if($this->_oUser->isLoggedIn()) {
            $this->_prepareCharList(); // prepares our character list
            $aInfos = $this->_getCharactersInfo();

            $this->_oView->assign('aInfos', $aInfos);

            $oCharModel = new DbCharModel();

            if($_POST['send'] == "reset") {
                $oCharModel->resetStatById($_POST['char']);
                $this->_oView->assign("success_{$_POST['char']}", "Character resetted");
            }

            $this->_oView->assign('page', 'page/account/reset_character.tpl');
            $this->_oView->assign('sAccountActive', 'active');    // sets menu point to active state
            $this->_oView->display('layout/base.tpl');
        } else {
            $this->_oView->assign('error', "You have to login first");
            $this->_oView->display('error_login.tpl');
        }
    }

    /**
     * Prepares the char list of user
     */
    private function _prepareCharList()
    {
        $oDbUser = new DbUserModel();
        $aCharList = $oDbUser->getCharIdList($this->_oUser->getUid());

        foreach($aCharList as $oPos) {
            $this->_aCharacterIdsList[] = $oPos->CharID;
        }
    }

    /**
     * Gets character info
     *
     * @return array
     */
    private function _getCharactersInfo()
    {
        $oCharModel = new DbCharModel();
        $aInfos = array();

        foreach($this->_aCharacterIdsList as $sId) {
            $oChar = $oCharModel->getCharById($sId);

            $aInfos[] = $oChar;
        }

        return $aInfos;
    }

    /**
     * Checks if data are complete
     *
     * @return bool
     */
    private function _isDataComplete() {
        return (!empty($this->_aPost['change_email']) || (!empty($this->_aPost['change_password']) && !empty($this->_aPost['change_password_repeat'])));
    }

    /**
     * Matches password
     *
     * @return bool
     */
    private function _isPasswordMatch() {
        return ($this->_aPost['change_password'] == $this->_aPost['change_password_repeat']);
    }


    /**
     * Checks email already exists
     *
     * @return bool
     */
    private function _isEmailTaken() {
        $oUser = new DbUserModel();
        $sEmail = $oUser->getUserByEmail($this->_aPost['change_email']);

        return !empty($sEmail);
    }

    /**
     * @return mixed
     */
    private function _isEmailWellFormatted()
    {
        return preg_match('/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/', $this->_aPost['change_email']);
    }

    /**
     * Prepares an reseted user to combine old data with reseted data
     * Logs data to log file if user reset a part.
     *
     * This functionality is very important for registered users,
     * without that function user wont be able to reset their characters.
     */
    public function updateuserdataAction($aParams)
    {
        if($this->_oUser->isLoggedIn()) {
            $oDbUser = new DbUserModel();
            $aCharList = $oDbUser->getCharIdList($this->_oUser->getUid());

            foreach($aCharList as $oPos) {
                $this->_aCharacterIdsList[] = $oPos->CharID;
            }

            $oCharModel = new DbCharModel();
            $this->_writeLog($oCharModel);

            $iCount = 0;

            // logs user data with echos!
            if(isset($aParams['id']) && !empty($aParams['id'])) {
                echo "<br>What's updated: <br>";
                echo "<span style='color: green;'>";
                if(isset($aParams['level'])) {
                    $oCharModel->setCurLevel($aParams['level'], $aParams['id']);
                    echo "- Level updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'level' too!</span><br>";
                }
                if(isset($aParams['gold'])) {
                    $oCharModel->setGold($aParams['gold'], $aParams['id']);
                    echo "- Gold updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'gold' too!</span><br>";
                }
                if(isset($aParams['hp'])) {
                    $oCharModel->setHp($aParams['hp'], $aParams['id']);
                    echo "- Hp updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'hp' too!</span><br>";
                }
                if(isset($aParams['mp'])) {
                    $oCharModel->setMp($aParams['mp'], $aParams['id']);
                    echo "- Mp updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'mp' too!</span><br>";
                }
                if(isset($aParams['str'])) {
                    $oCharModel->setStrength($aParams['str'], $aParams['id']);
                    echo "- Str updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'str' too!</span><br>";
                }
                if(isset($aParams['int'])) {
                    $oCharModel->setIntellect($aParams['int'], $aParams['id']);
                    echo "- Int updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'int' too!</span><br>";
                }
                if(isset($aParams['inventory'])) {
                    $oCharModel->setInventorySize($aParams['inventory'], $aParams['id']);
                    echo "- Inventory updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'inventory' too!</span><br>";
                }
                if(isset($aParams['hunterlvl'])) {
                    $oCharModel->setHunterLevel($aParams['hunterlvl'], $aParams['id']);
                    echo "- Hunter level updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'hunterlvl' too!</span><br>";
                }
                if(isset($aParams['robberlvl'])) {
                    $oCharModel->setRooberLevel($aParams['robberlvl'], $aParams['id']);
                    echo "- Robber level updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'robberlvl' too!</span><br>";
                }
                if(isset($aParams['traderlvl'])) {
                    $oCharModel->setTraderLevel($aParams['traderlvl'], $aParams['id']);
                    echo "- Trader level updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'traderlvl' too!</span><br>";
                }
                if(isset($aParams['skill'])) {
                    $oCharModel->setSkillpoints($aParams['skill'], $aParams['id']);
                    echo "- Skills updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'skill' too!</span><br>";
                }
                if(isset($aParams['stat'])) {
                    $oCharModel->setStatpoints($aParams['stat'], $aParams['id']);
                    echo "- Stats updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'stat' too!</span><br>";
                }
                if(isset($aParams['silk'])) {
                    $oCharModel->setSilk($aParams['silk'], $this->_oUser->getUid());
                    echo "- Silk updated<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'silk' too!</span><br>";
                }
                if(isset($aParams['deletesilk'])) {
                    $oCharModel->setSilkEntry($this->_oUser->getUid());
                    echo "- Silk entry deleted<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'deletesilk' too!</span><br>";
                }
                if(isset($aParams['resetskill'])) {
                    $oCharModel->resetSkillById($aParams['resetskill']);
                    echo "- Skills resetted<br>";
                    $iCount++;
                } else {
                    echo "<span style='color: red;'>- You can update 'resetskill' too!</span><br>";
                }
                echo "</span>";

                echo "<br>_____________________________________<br>";
                echo "<br>".$iCount." columns updated!<br>";
                echo "<span style='color: #0017ff;'>Updated character: " .$oCharModel->getCharById($aParams['id'])->CharName16."</span><br>";

                die("<br><span style='color: green;'>Update successful!</span>");
            }

            die("<br><span style='color: red;'>Char NOT updated!</span>");
        } else {
            echo "You can't see data? You need to login!<br>";
        }
    }

    /**
     * Writes the log file
     *
     * @param $oCharModel
     */
    private function _writeLog($oCharModel)
    {
        if($this->_oUser->isLoggedIn()) {
            if(empty($this->_aCharacterIdsList)) {
                echo "<h3 style='color: red;'>Current user has no characters!</h3>";
            }
            echo "<table border='1'>";
            echo "<th>id</th>";
            echo "<th>nickname</th>";
            echo "<th>second nickname</th>";
            foreach($this->_aCharacterIdsList as $sId) {

                echo "<tr>";
                echo "<td style='width: 100px;'>".$sId."</td>";

                foreach($oCharModel->getCharnames($sId) as $sName) {
                    echo "<td style='width: 100px;'>".$sName."</td>";
                }
                echo "</tr>";

            }
            echo "</table>";
        }
    }
}
