<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 27.04.2015
 * @time: 19:13
 * @copyright: 2015 solutionDrive GmbH
 */
class LotteryController extends BaseController
{
    protected $_iRuns          = 1;
    protected $_iChancePercent = 0;

    private $_iStart    = 1;
    private $_iEnd      = 100;

    /**
     * Checks if some one won.
     */
    public function hasWon()
    {
        for ($i = 1; $i <= $this->getRuns(); $i++) {
            $rand = rand($this->_iStart, $this->_iEnd);

            if ($rand <= $this->getChancePercent()) {
                return true;
            }

            return false;
        }
    }

    /**
     * Sets the percentage of lottery win
     *
     * @param $iPercent
     */
    public function setChancePercent($iPercent)
    {
        $this->_iChancePercent = $iPercent;
    }

    /**
     * Gets the percentage of lottery win
     *
     * @return int
     */
    public function getChancePercent()
    {
        return $this->_iChancePercent;
    }

    /**
     * Sets the runs of lottery win
     *
     * @param $iRuns
     */
    public function setRuns($iRuns)
    {
        $this->_iChancePercent = $iRuns;
    }

    /**
     * Gets the runs of lottery win
     *
     * @return int
     */
    public function getRuns()
    {
        return $this->_iRuns;
    }
}