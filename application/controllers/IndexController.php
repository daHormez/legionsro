<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class IndexController extends BaseController
{

    /**
     * IndexAction
     */
    public function indexAction()
    {
        $oNews = new AdminNewsModel();
        $this->_oView->assign("aNews", $oNews->getNews());

        // Fortress
        $oDbFortressModel = new DbFortressModel();
        $oDbGuildModel = new DbGuildModel();

        $oJangonFortress = $oDbFortressModel->getFortressById(1);
        $oJangonGuild = $oDbGuildModel->getGuildById($oJangonFortress->GuildID);
        $this->_oView->assign('sJanganGuild', $oJangonGuild->Name);

        $oBanditFortress = $oDbFortressModel->getFortressById(3);
        $oBanditGuild = $oDbGuildModel->getGuildById($oBanditFortress->GuildID);
        $this->_oView->assign('sBanditGuild', $oBanditGuild->Name);

        $oConstFortress = $oDbFortressModel->getFortressById(4);
        $oConstGuild = $oDbGuildModel->getGuildById($oConstFortress->GuildID);
        $this->_oView->assign('sConstGuild', $oConstGuild->Name);

        $oHotanFortress = $oDbFortressModel->getFortressById(6);
        $oHotanGuild = $oDbGuildModel->getGuildById($oHotanFortress->GuildID);
        $this->_oView->assign('sHotanGuild', $oHotanGuild->Name);

        $this->_oView->assign('page', 'page/news.tpl');
        $this->_oView->assign('sNewsActive', 'active');    // sets menu point to active state
        $this->_oView->display('layout/base.tpl');
    }

    /**
     * logs the user out!
     */
    public function logoutAction()
    {
        SessionController::logout();

        if (!SessionController::isLoggedIn()) {
            header('Location: http://' . $_SERVER['SERVER_NAME'] . $this->_oConfig->getVar("baseurl"));
        }
    }

    /**
     * Reads the config file
     */
    public function read_config_fileAction()
    {
        $oConfig = ConfigController::getInstance();
        $oConfig->read();
    }

    public function read_account_dbAction()
    {
        DatabaseAccountController::read();
    }

    public function read_shard_dbAction()
    {
        DatabaseShardController::read();
    }

    public function test_paypal_insertAction()
    {
        define("LOG_FILE", "./payment_test.log");

        $timenow = time();
        $sSql = "INSERT INTO paypal (txn_id, payer_email, mc_gross, username, date) VALUES ('0', 'hormess@solutiondrive.de', 100, 'default', '{$timenow}')";
        $oStm = $this->_oDbAccount->prepare($sSql);
        $blExec = $oStm->execute();

        if (!$blExec) {
            error_log("- ".$this->_oConfig->getVar("paypal_email").' -- In table paypal is no new entry!'.PHP_EOL, 3, LOG_FILE);
        } else {
            error_log("- ".$this->_oConfig->getVar("paypal_email").' -- In table paypal is a new entry txn_id = 0 payer_email = hormess@solutiondrive.de'.PHP_EOL, 3, LOG_FILE);
        }

        $sSql = "exec CGI.CGI_WebPurchaseSilk '0', 'username', 0, 100, 0"; // will execute automated in game update of silks.
        $oStm = $this->_oDbAccount->prepare($sSql);
        $blExec = $oStm->execute();

        if (!$blExec) {
            error_log("- ".$this->_oConfig->getVar("paypal_email").' -- No Silk gained!'.PHP_EOL, 3, LOG_FILE);
        } else {
            error_log("- ".$this->_oConfig->getVar("paypal_email").' -- Silk gained and in game updated'.PHP_EOL, 3, LOG_FILE);
        }
    }
}
