<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 24.04.2015
 * @time: 15:16
 * @copyright: 2015 solutionDrive GmbH
 */
class AdminController extends BaseController
{
    private $_aPost = array();

    /**
     * IndexAction
     */
    public function indexAction()
    {
        if($this->_checkStatus()) {
            $this->_oView->assign('page', 'admin/main.tpl');
        }

        $this->_oView->display('layout/base.tpl');
    }

    /**
     * NewsAction
     */
    public function newsAction()
    {
        $this->_aPost = $_POST;

        if($this->_checkStatus()) {
            $oAdminNewsModel = new AdminNewsModel();

            if(!empty($this->_aPost)) {
                if($this->_isDataComplete()) {
                    // post news
                    $oAdminNewsModel->postNews($this->_aPost['admin_news_type'], $this->_aPost['admin_news_heading'], nl2br(stripcslashes($this->_aPost['admin_news_text'])));
                    $this->_oView->assign('success', 'News successful posted');
                } elseif(!$this->_isDataComplete() && $this->_aPost['send'] == "newsdelete") {
                    $oAdminNewsModel->deleteNews($this->_aPost['del_id']);
                    $this->_oView->assign('success', 'News successful deleted');
                } else {
                    $this->_oView->assign('error', 'Please fill all fields to post news');
                }
            }

            $this->_oView->assign('aNews', $oAdminNewsModel->getNews());
            $this->_oView->assign('page', 'admin/news.tpl');
        }

        $this->_oView->display('layout/base.tpl');
    }

    /**
     * Checks the status
     *
     * @return bool
     */
    private function _checkStatus()
    {
        if($this->_oUser->isLoggedIn()) {
            $oDbUser = new DbUserModel();

            if($oDbUser->isAdmin($this->_oUser->getUid())) {
                return true;
            } else {
                $this->_oView->assign('error', 'You have to be an Admin to view this page');
                $this->_oView->assign('page', 'admin/error.tpl');
            }
        } else {
            $this->_oView->assign('error', 'Login first');
            $this->_oView->assign('page', 'admin/error.tpl');
        }

        return false;
    }

    /**
     * Checks if data are complete
     *
     * @return bool
     */
    private function _isDataComplete() {
        return (!empty($this->_aPost['admin_news_type']) &&
                !empty($this->_aPost['admin_news_heading']) &&
                !empty($this->_aPost['admin_news_text']));
    }
}