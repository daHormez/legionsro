<?php

/**
 * @author Daniel Hormeß <hormess@solutiondrive.de>
 * @date   11.11.2014
 */
class ViewController
{
    protected static $_oInstance = null; // View-Objekt (Smarty) der Applikation

    /**
     * Gets the instance
     *
     * @param string : static $sConfDir config directory
     * @param string : static $sTmpDir  tmp directory
     *
     * @return object
     */
    public static function getInstance($sConfDir, $sTmpDir)
    {
        if (!isset(static::$_oInstance)) {
            include_once 'libs/Smarty.class.php';

            $oViewModel = new ViewModel;

            $sCompileDir = $sTmpDir . 'templates_c/';
            $sCacheDir = $sTmpDir . 'cache/';

            // Verzeichnisse ggf. anlegen
            $aTmpDirs = array($sTmpDir, $sCompileDir, $sCacheDir);

            foreach ($aTmpDirs as $sDir) {
                if (!file_exists($sDir)) {
                    mkdir($sDir);
                    chmod($sDir, 0777);
                }
            }

            $oViewModel->template_dir = 'application/views/';
            $oViewModel->config_dir = $sConfDir;
            $oViewModel->compile_dir = $sCompileDir;
            $oViewModel->cache_dir = $sCacheDir;

            static::$_oInstance = $oViewModel;
        }

        return static::$_oInstance;   // liefert View-Objekt
    }
}
