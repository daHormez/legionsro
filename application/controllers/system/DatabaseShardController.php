<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 28.01.2015
 * @time: 08:42
 * @copyright: 2015 solutionDrive GmbH
 */
class DatabaseShardController
{
    protected static $_oInstance    = null; // Datenbankobjekt der Applikation
    protected static $_aTablenames  = null; // Tabellennamen

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (!isset(static::$oInstance)) {
            $oConfig = ConfigController::getInstance();

            $sHost =  $oConfig->getVar('dbhost');
            $sUser =  $oConfig->getVar('dbuser');
            $sPass =  $oConfig->getVar('dbpass');
            $sDb   =  $oConfig->getVar('dbname_shard');

            static::$_oInstance = new PDO("sqlsrv:server=$sHost;database=$sDb","$sUser","$sPass");
        }

        return static::$_oInstance;  // liefert PDO-Objekt
    }

    /**
     * Gets all tablenames
     *
     * @return null
     */
    public static function getTablesNames()
    {
        if (static::$_aTablenames) {
            return static::$_aTablenames;
        }

        $oDb = DatabaseShardController::getInstance();

        $oStm  = $oDb->prepare('SELECT name FROM sys.Tables');
        $oStm->execute();
        $aTablenames  = $oStm->fetchAll();

        static::$_aTablenames = array();

        foreach ($aTablenames as $aTablename) {
            static::$_aTablenames[] = strtolower(array_shift($aTablename));
        }

        return static::$_aTablenames;
    }

    /**
     * Checks if table exists
     *
     * @param string $sTablename a Tablename
     *
     * @return bool
     */
    public static function checkTableExists($sTablename)
    {
        $aTablenames = static::getTablesNames();

        return in_array(strtolower($sTablename), $aTablenames); // existiert Tabellenname in aktueller DB?
    }

    /**
     * Reads db
     */
    public static function read()
    {
        $aNames = self::getTablesNames();

        foreach ($aNames as $sName) {
            echo "- " . $sName . "<br>";
        }
    }
}
