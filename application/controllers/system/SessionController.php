<?php

/**
 * @author Daniel Hormeß <hormess@solutiondrive.de>
 * @date   26.11.2014
 */
class SessionController
{

    /**
     * @param ViewController $oView a view object
     */
    public function __construct($oView)
    {
        if(session_status() != 2) {
            session_start();
        }

        return $this->login($oView);
    }

    /**
     * @param ViewController $oView a view object
     *
     * @return UserModel
     */
    public function login($oView)
    {
        if (empty($_POST['username']) && empty($_POST['password']) && $_POST['send'] == "login") {
            $oView->assign('error', "Please type Username and Password!");
            $oView->display('error_login.tpl');

            exit();
        }

        if (!empty($_POST['username']) || !empty($_POST['password'])) {
            if (!isset($_SESSION['uid']) || !trim($_SESSION['uid'])) {
                if ($_POST['username'] && $_POST['password']) {
                    $sUid = UserModel::authenticateUser($_POST['username'], $_POST['password']);

                    if ($sUid == 1) {
                        $oView->assign('error', "User doesn't exists or Password is wrong!");
                        $oView->display('error_login.tpl');

                        exit();
                    }
                } else {
                    if ($_POST['username'] && !$_POST['password']) {
                        $oView->assign('error', "Please type password!");
                    } elseif (!$_POST['username'] && $_POST['password']) {
                        $oView->assign('error', "Please type username!");
                    }

                    $oView->display('error_login.tpl');

                    exit();
                }

                $_SESSION['uid'] = $sUid;
            }

            $sUid = $_SESSION['uid'];

            return new UserModel($sUid);
        }
    }

    /**
     * logs the user out
     */
    public static function logout()
    {
        unset($_SESSION['uid']);
        session_destroy();
    }

    /**
     * checks if user logged in
     *
     * @return boolean
     */
    public static function isLoggedIn()
    {
        return isset($_SESSION['uid']);
    }

    /**
     * Checks if user is admin
     *
     * @return bool
     */
    public static function isAdmin()
    {
        if(self::isLoggedIn()) {
            $oUser = new DbUserModel();
            return $oUser->isAdmin(self::getUid());
        }

        return false;
    }

    /**
     * Gets the uId
     *
     * @return mixed
     */
    public static function getUid()
    {
        return $_SESSION['uid'];
    }
}
