<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class ConfigController
{

    protected static $_oInstance = null;              // config-Object der Application

    private static $_sConfigDir = 'configs/';
    private static $_sConfigFile = 'config.conf';

    protected $_aData = array();
    protected $_sNamespace = '';

    /**
     * Gets the instance
     *
     * @return object
     */
    public static function getInstance()
    {
        if (!isset(static::$_oInstance)) {
            static::$_oInstance = new ConfigController();
            static::$_oInstance->_parseConfigFile();
        }

        return static::$_oInstance;   // Config-Objekt
    }

    /**
     * Reads config file
     */
    public function read()
    {
        echo "<table>";
        foreach($this->_aData as $sNamespace => $sValue) {
            echo "<tr><td width='350'>".$sNamespace."</td><td>".$sValue."</td></tr>";
        }
        echo "</Table>";
    }

    /**
     * Parses config file
     *
     * @throws Exception
     */
    private function _parseConfigFile()
    {
        $sConfigPath = static::$_sConfigDir . static::$_sConfigFile;

        if (!file_exists($sConfigPath)) {
            throw new Exception('Die Konfigurationsdatei ist nicht vorhanden!');
        }

        $aConfigFile = file($sConfigPath);

        foreach ($aConfigFile as $iLineNumber => $sLine) {
            $sLine = trim($sLine);

            if (!$sLine) {
                continue; // leere Zeile
            }
            if (substr($sLine, 0, 1) == '#') {
                continue; // Kommentar
            }

            $iAssignPos = strpos($sLine, '=');
            if ($iAssignPos === false) {
                $iLineNumber++;
                throw new Exception("Kein Zuweisungsoperator in Zeile $iLineNumber.");
            }

            $sKey = trim(substr($sLine, 0, $iAssignPos));
            $sValue = trim(substr($sLine, $iAssignPos + 1));

            if (strpos($sValue, "!#") !== false) {
                $sValue = "";
                //throw new Exception("Auskommentierte value. $sValue");
            }

            if (strcasecmp($sKey, 'namespace') === 0) {
                $this->_sNamespace = strtolower($sValue);
            }

            $this->_aData[$sKey] = $sValue;
        }
    }

    /**
     * Sets the Namespace to configure different projects
     *
     * @param string $sNamespace Ein Namespace
     */
    public function setNamespace($sNamespace)
    {
        $this->_sNamespace = strtolower((string) $sNamespace);
    }

    /**
     * Gets the namespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->_sNamespace;
    }

    /**
     * Checks the Variable
     *
     * @param string $sVarname   Variablenname
     * @param bool   $blStrict   Strikt nach Namen suchen
     * @param string $sNamespace Ein Namespace
     *
     * @return bool
     */
    public function issetVar($sVarname, $blStrict = false, $sNamespace = '')
    {
        if (!$sNamespace) {
            $sNamespace = $this->_sNamespace;
        }

        // to build exception message..
        if ($blStrict && $sNamespace) {
            return isset($this->_aData[$sNamespace . ':' . $sVarname]);
        } else {
            return isset($this->_aData[$sNamespace . ':' . $sVarname]) || isset($this->_aData[$sVarname]);
        }
    }

    /**
     * Gets the variable from config
     *
     * @param string $sVarname   Variablenname
     * @param string $sNamespace Ein Namespace
     * @param string $sDefault   Default Wert
     *
     * @return string
     * @throws Exception
     */
    public function getVar($sVarname, $sNamespace = '', $sDefault = null)
    {
        if (!$sNamespace) {
            $sNamespace = $this->_sNamespace;
        }

        $sNamespaceVarname = $sNamespace ? $sNamespace . ':' . $sVarname : $sVarname;

        if (!isset($this->_aData[$sVarname])
            && !isset($this->_aData[$sNamespaceVarname])
        ) {
            if ($sDefault !== null) {
                return $sDefault;
            }

            $sVarnames = $sNamespace ? "$sNamespaceVarname'/'$sVarname" : $sVarname;

            throw new Exception("Konfigurationsvariable '$sVarnames' ist nicht gesetzt.");
        }

        return isset($this->_aData[$sNamespaceVarname]) ? $this->_aData[$sNamespaceVarname] : $this->_aData[$sVarname];
    }
}
