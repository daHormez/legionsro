<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class BaseController
{
    protected $_oView    = null; // View-Object
    protected $_oConfig  = null; // Config-Object
    protected $_sTmpdir  = null; // Temp-Dir
    protected $_sConfdir = null; // Config-Dir
    protected $_oUser    = null; // User-Object (loggedin User)
    protected $_oDbAccount = null; // Database-Object-Account
    protected $_oDbShard   = null; // Database-Object-Shard
    protected $_oDbUser    = null; // Database-User-object short: current user

    /**
     * Basic construct
     */
    public function __construct()
    {
        $this->_sTmpdir = 'tmp/';
        $this->_sConfdir = 'configs/';

        $this->_oView = ViewController::getInstance($this->_sConfdir, $this->_sTmpdir);
        $this->_oDbAccount = DatabaseAccountController::getInstance();
        $this->_oDbShard = DatabaseShardController::getInstance();
        $this->_oConfig = ConfigController::getInstance();

        // sets the general variables
        $this->setGeneralProjectVariables();
        $this->setGeneralViewVariables();

        $this->_oUser = new SessionController($this->_oView); // Session-Check

        $this->_oView->assign("blLoggedIn", $this->_oUser->isLoggedIn());
        $this->_oView->assign("blAdmin", $this->_oUser->isAdmin());

        $this->setCurrentUser();
        $this->setCurrentUserCount();

        $blOnline = $this->getServerStatus();
        $this->_oView->assign("blOnline", $blOnline);
    }

    /**
     * analyzes our URL
     *
     * @param string $sUrl A Url
     *
     * @return array
     */
    public static function analyzeUrl($sUrl)
    {
        $aUrlParts = explode('/', $sUrl);
        $iPartCount = count($aUrlParts);

        array_walk(
            $aUrlParts,
            function (&$value, &$key) {
                $value = urldecode(ucfirst($value));
            }
        );

        switch ($iPartCount) {
            case 1:
                if ($aUrlParts[0] == '') {
                    $sController = 'Index';
                } else {
                    $sController = $aUrlParts[0];
                }

                $sAction = "index";
                $aParams = array();
                break;
            case 2:
                $sController = ucfirst(strtolower($aUrlParts[0]));
                $sAction = strtolower($aUrlParts[1]);

                if (!trim($sAction)) {
                    $sAction = 'index';
                }

                $aParams = array();
                break;
            default:
                $sController = ucfirst(strtolower($aUrlParts[0]));
                $sAction = lcfirst(strtolower($aUrlParts[1]));

                $aParams = array();

                for ($iCount = 2; $iCount < $iPartCount; $iCount++) {
                    $sKey = $aUrlParts[$iCount];
                    $iCount++;
                    if ($iCount < $iPartCount) {
                        $sVal = $aUrlParts[$iCount];
                        if (!trim($sVal)) {
                            $sVal = null;  // Leeren String => null
                        }
                    } else {
                        $sVal = null;
                    }

                    $aParams[lcfirst($sKey)] = strtoupper($sVal);
                }
        }

        return array($sController . 'Controller', $sAction . 'Action', $aParams);
    }

    /**
     * sets the general view variables
     */
    protected function setGeneralViewVariables()
    {
        $this->_oView->assign('baseurl', SITE_BASE_URL);
        $this->_oView->assign('projectname', $this->_oConfig->getVar("projectname"));
        $this->_oView->assign('sPaypalEmail', $this->_oConfig->getVar("paypal_email"));

        $sCountDown = $this->getCountdown();
        $this->_oView->assign('sCountdown', $sCountDown);
        $this->_oView->assign('sCountdownMessage', $this->_oConfig->getVar("countdown_message"));

        $this->_oView->assign('iMaxUsers', $this->_oConfig->getVar("maxusers"));
        $this->_oView->assign('sCrowdedCount', $this->_oConfig->getVar("crowded"));
        $this->_oView->assign('sPopularCount', $this->_oConfig->getVar("popular"));

        $this->_oView->assign('sStatExp', $this->_oConfig->getVar("stat_exp"));
        $this->_oView->assign('sStatSp', $this->_oConfig->getVar("stat_sp"));
        $this->_oView->assign('sStatItem', $this->_oConfig->getVar("stat_item"));
        $this->_oView->assign('sStatItemSox', $this->_oConfig->getVar("stat_item_sox"));
        $this->_oView->assign('sStatGold', $this->_oConfig->getVar("stat_gold"));
    }

    /**
     * Gets the countdown
     *
     * @return string
     */
    public function getCountDown()
    {
        $sStart = strtotime($this->_oConfig->getVar("countdown")) - time();
        $sDay  = floor($sStart / 86400);
        $sHour = floor(($sStart % 86400) / 3600);
        $sMin  = floor(($sStart % 3600) / 60);

        $sCountDown = "";

        if($sDay) {
            $sCountDown .= "$sDay Days ";
        }
        if($sHour) {
            $sCountDown .= "$sHour Hours ";
        }
        if($sMin) {
            $sCountDown .= "$sMin Minutes ";
        }

        return $sCountDown;
    }

    /**
     * Sets current User
     */
    private function setCurrentUser()
    {
        $oDbUser = new DbUserModel();
        $this->_oDbUser = $oDbUser;

        $oUser = $oDbUser->getUserById($this->_oUser->getUid());
        $this->_oView->assign("oUser", $oUser);
    }

    /**
     * Sets current User
     */
    private function setCurrentUserCount()
    {
        $oDbCurrentUser = new DbCurrentUserModel();
        $iCount = $oDbCurrentUser->getCount();

        $sUserCount = $iCount->nUserCount;

        if (!$sUserCount) {
            $sUserCount = "0";
        }

        $this->_oView->assign('sUserCount', $sUserCount);
    }

    /**
     * Gets the server status
     *
     * @return bool
     */
    public function getServerStatus()
    {
        $sIp   = $this->_oConfig->getVar("server_ip");      // Server IP
        $sPort = $this->_oConfig->getVar("server_port");    // Server port

        $blFp = @fsockopen($sIp, $sPort, $errno, $errstr, 2);

        if ($blFp) {
            return true; // Online
        }

        return false; // Offline
    }

    /**
     * sets the general project variables
     */
    protected function setGeneralProjectVariables()
    {
        $sPort = (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443) ? (':' . $_SERVER['SERVER_PORT']) : '';
        if(!defined('SITE_BASE_URL')) {
            define('SITE_BASE_URL', 'http://' . $_SERVER['SERVER_NAME'] . $sPort . $this->_oConfig->getVar("baseurl"));
        }
    }
}
