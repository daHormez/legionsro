<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class RegisterController extends BaseController
{
    private $_aPost = array();

    /**
     * IndexAction
     */
    public function indexAction()
    {
        if($_POST['send'] == "register") {
            $this->_aPost = $_POST;

            $this->_oView->assign("aPost", $this->_aPost);
            $blRegister = $this->_validateRegisterUser();
        }

        if($blRegister) {
            $this->registerUser();

            $this->_oView->assign('sUsername', ucfirst($this->_aPost['reg_username']));
            $this->_oView->assign('page', 'page/register_success.tpl');
        } else {
            $this->_oView->assign('page', 'page/register.tpl');
        }

        $this->_oView->assign('sRegisterActive', 'active');    // sets menu point to active state
        $this->_oView->display('layout/base.tpl');
    }

    public function registerUser()
    {
        $oDbUser = new DbUserModel();

        try {
            $blGenerated = $oDbUser->generateNewUser(
                $this->_aPost['reg_username'],
                md5($this->_aPost['reg_password']),
                $this->_aPost['reg_email']
            );

            if(!$blGenerated) {
                throw new ClassfileException("User couldn't be added!");
            }
        } catch (ClassfileException $oEx) {
            die($oEx->getMessage());
        }
    }

    /**
     * Validates Register User
     *
     * @return bool
     */
    private function _validateRegisterUser()
    {
        if($this->_isDataComplete()) {
            // check user already exists
            if($this->_isUsernameTaken()) {
                $this->_oView->assign("error", "Username already exists");
                $blAnyError = true;
            } elseif($this->_isUsernameWellFormatted() <= 0) {
                $this->_oView->assign("error", "Type correct Username (Only use numbers and letters)");
                $blAnyError = true;
            }

            // check email already exists
            if($this->_isEmailTaken()) {
                $this->_oView->assign("error", "E-Mail already exists");
                $blAnyError = true;
            } elseif($this->_isEmailWellFormatted() <= 0) {
                $this->_oView->assign("error", "Type correct E-Mail");
                $blAnyError = true;
            }

            // check password repeat
            if(!$this->_isPasswordMatch()) {
                $this->_oView->assign("error", "Passwords don't match");
                $blAnyError = true;
            }

            // check terms of use
            if(!$this->_hasTermsAccepted()) {
                $this->_oView->assign("error", "Please accept the terms");
                $blAnyError = true;
            }

            if($blAnyError) {
                return false;
            }

            return true;
        } else {
            $this->_oView->assign("error", "Fill all fields to complete registration");

            return false;
        }
    }

    /**
     * Checks if data are complete
     *
     * @return bool
     */
    private function _isDataComplete() {
        return (!empty($this->_aPost['reg_username']) &&
                !empty($this->_aPost['reg_email']) &&
                !empty($this->_aPost['reg_password']) &&
                !empty($this->_aPost['reg_password_repeat']));
    }

    /**
     * Matches password
     *
     * @return bool
     */
    private function _isPasswordMatch() {
        return ($this->_aPost['reg_password'] == $this->_aPost['reg_password_repeat']);
    }

    /**
     * Checks terms
     *
     * @return bool
     */
    private function _hasTermsAccepted() {
        return ($this->_aPost['terms'] == "on");
    }

    /**
     * Checks username already exists
     *
     * @return bool
     */
    private function _isUsernameTaken() {
        $oUser = new DbUserModel();
        $sUsername = $oUser->getUserByUsername($this->_aPost['reg_username']);

        return !empty($sUsername);
    }

    /**
     * Checks email already exists
     *
     * @return bool
     */
    private function _isEmailTaken() {
        $oUser = new DbUserModel();
        $sEmail = $oUser->getUserByEmail($this->_aPost['reg_email']);

        return !empty($sEmail);
    }

    /**
     * @return mixed
     */
    private function _isEmailWellFormatted()
    {
        return preg_match('/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/', $this->_aPost['reg_email']);
    }

    /**
     * @return int
     */
    private function _isUsernameWellFormatted()
    {
        return preg_match('/^[a-zA-Z0-9]+$/', $this->_aPost['reg_username']);
    }
}
