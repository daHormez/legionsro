<?php
/**
 * Created by solutionDrive GmbH
 *
 * @author: Daniel Hormeß <Hormess@solutionDrive.de>
 * @date: 25.04.2015
 * @time: 08:42
 * @copyright: 2015 solutionDrive GmbH
 */
class RankingController extends BaseController
{
    /**
     * Index Action
     */
    public function indexAction()
    {
        // char model
        $oChar = new DbCharModel();
        $this->_oView->assign('oChar', $oChar);
        $this->_oView->assign('sTotalCharCount', $oChar->getTotalCharCount()->TotalCharCount);
        $this->_oView->assign('sTotalAccountCount', $this->_oDbUser->getTotalAccountCount()->TotalAccountCount);

        // job model
        $oDbCharJob = new DbCharJobModel();
        $oTrader = $oDbCharJob->getAllTrader();
        $oRobber = $oDbCharJob->getAllRobber();
        $oHunter = $oDbCharJob->getAllHunter();

        $this->_oView->assign('oTrader', $oTrader);
        $this->_oView->assign('oRobber', $oRobber);
        $this->_oView->assign('oHunter', $oHunter);

        // guild model
        $oDbGuild = new DbGuildModel();
        $oGuild = $oDbGuild->getAllGuilds();

        $this->_oView->assign('oGuild', $oGuild);

        // default pages
        $this->_oView->assign('page', 'page/ranking.tpl');
        $this->_oView->assign('sRankingActive', 'active');
        $this->_oView->display('layout/base.tpl');
    }
}