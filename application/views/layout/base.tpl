<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <link rel="icon" href="{$baseurl}/favicon.ico">
        
        <title>{$projectname}</title>

        <!-- Bootstrap -->
        <link href="{$baseurl}/out/src/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{$baseurl}/out/src/css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body id="bg_contents">

        {if $blAdmin}
            <div class="container">
                {include file="admin/admin_nav.tpl"}
            </div>
        {/if}

        <div class="container">
            {include file="layout/above_header.tpl"}
        </div>

        <div class="container">
            {include file='layout/header.tpl'}
        </div>

        <div class="container">
            {include file="layout/under_header.tpl"}
        </div>

        <div class="container">
            {include file=$page}
        </div>
            
        <div class="container">
            {include file='layout/footer.tpl'}
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{$baseurl}/out/src/js/jquery-1.11.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{$baseurl}/out/src/js/bootstrap.min.js"></script>
        <script src="{$baseurl}/out/src/js/plugins/main.js"></script>
    </body>
</html>



    

    