<div class="col-md-4 under_header_height">
    <div class="col-md-12 stats-box">
        <p>Server Status</p>
        {if $blOnline}
            <span style="color: green;" class="server_online_stat">ONLINE</span>
        {else}
            <span style="color: red;" class="server_online_stat">OFFLINE</span>
        {/if}
    </div>
</div>

<div class="col-md-4 under_header_height">
    <div class="col-md-12 stats-box">
        <p>{$sCountdownMessage}</p>
        <span class="countdown_stat">{$sCountdown}</span>
    </div>
</div>

<div class="col-md-4 under_header_height">
    <div class="col-md-12 stats-box">
        <p>Current Online</p>

        {if $sUserCount >= $sCrowdedCount}
            <span style="color: red;" class="player_online_stat">
                {$sUserCount} / {$iMaxUsers}
            </span>
        {elseif $sUserCount >= $sPopularCount && $sUserCount < $sCrowdedCount}
            <span style="color: orange;" class="player_online_stat">
                {$sUserCount} / {$iMaxUsers}
            </span>
        {else}
            <span style="color: green;" class="player_online_stat">
                {$sUserCount} / {$iMaxUsers}
            </span>
        {/if}
    </div>
</div>