<div class="col-md-12 content_bg_color">
    <div class="form-group">
        <ol class="breadcrumb">
            <li><a href="{$baseurl}/admin">Admin main</a></li>
            <li class="active">News</li>
        </ol>
    </div>

    <div class="col-md-6">
        <h3>Update news</h3>

        <form action="" method="post">
            <div class="form-group">
                <select class="form-control" name="admin_news_type">
                    <option value="Notice">Notice</option>
                    <option value="Announcement">Announcement</option>
                    <option value="Update">Update</option>
                </select>
            </div>

            <div class="form-group">
                <input class="form-control" type="text" name="admin_news_heading" placeholder="News title" value="">
            </div>

            <div class="form-group">
                <textarea style="resize:vertical;" class="form-control" name="admin_news_text" placeholder="News text"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default">Post news</button>
                <input type="hidden" name="send" value="adminnews"/>
            </div>
        </form>
    </div>

    <div class="col-md-6">
        <h3>Delete news</h3>

        {if empty($aNews)}
            <div style="padding-bottom: 10px;" class="col-md-6">
                <span class="glyphicon glyphicon-exclamation-sign"></span> <span class="text-danger">No news at the moment</span>
            </div>
        {else}
            {foreach from=$aNews key=k item=i name=news}
                <div style="padding-bottom: 10px;" class="col-md-6">
                    <form action="" method="post">
                        <span class="glyphicon glyphicon-list-alt"></span> {ucfirst($i->HEADING)}
                        <button type="submit" class="btn btn-default btn-width-200">Delete</button>

                        <input type="hidden" name="send" value="newsdelete"/>
                        <input type="hidden" name="del_id" value="{$i->ID}"/>
                    </form>
                </div>
            {/foreach}
        {/if}
    </div>

    <div class="col-md-12" style="padding-top:10px;">
        {if $error}
            <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
        {/if}
        {if $success}
            <div class="alert alert-success"><i class="glyphicon glyphicon-check"></i> {$success}</div>
        {/if}
    </div>
</div>