<div style="padding-top:10px;">
    {if $error}
        <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
    {/if}
</div>