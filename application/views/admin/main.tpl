<div class="col-md-12 content_bg_color">
    <div class="form-group">
        <ol class="breadcrumb">
            <li class="active">Admin main</li>
        </ol>
    </div>

    <h3>Admin Main</h3>

    <a href="{$baseurl}/admin/news" class="btn btn-default btn-width-200">News config</a>

    <div style="padding-top:10px;">
        {if $error}
            <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
        {/if}
    </div>
</div>