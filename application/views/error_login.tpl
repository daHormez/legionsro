<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="{$baseurl}/favicon.ico">

    <title>{$projectname}</title>

    <!-- Bootstrap -->
    <link href="{$baseurl}/out/src/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{$baseurl}/out/src/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="login_error_screen">

<div class="container">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login_error_screen_box">
        <div style="padding-top: 20px; padding-bottom: 50px; text-align: center;">
            <div class="col-md-6 col-md-offset-3">
                <a href="{$baseurl}" class="btn btn-info" role="button">Back to site</a>
            </div>
        </div>

        <form role="form" method="post" action="">
            <!-- username start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" placeholder="Username (Case Sensitive)" value="{$savedLogin}" name="username" class="form-control" id="username">
                </div>
            </div>
            <!-- username end -->

            <!-- password start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" placeholder="Password" name="password" class="form-control" id="password">
                </div>
            </div>
            <!-- password end -->

            <button type="submit" class="btn btn-default">Anmelden</button>
            <a href="{$baseurl}/register" class="btn btn-info">Register</a>
            <input type="hidden" name="send" value="login"/>

            <div style="padding-top:10px;">
                {if $error}
                    <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
                {/if}
            </div>
        </form>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="out/src/js/jquery-1.11.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="out/src/js/bootstrap.min.js"></script>
</body>
</html>

