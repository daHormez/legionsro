<!-- Modal login start -->
<div class="modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Login</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="">
                    <!-- username start -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" placeholder="Username" value="{$savedLogin}" name="username" class="form-control" id="username">
                        </div>
                    </div>
                    <!-- username end -->

                    <!-- password start -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" placeholder="Password" name="password" class="form-control" id="password">
                        </div>
                    </div>
                    <!-- password end -->

                    <button type="submit" class="btn btn-default">Anmelden</button>
                    <input type="hidden" name="send" value="login"/>
                </form>
            </div>
            <!--<div class="modal-footer">
                <p>I am a login Footer text</p>
            </div>-->
        </div>
    </div>
</div>
<!-- Modal login end -->