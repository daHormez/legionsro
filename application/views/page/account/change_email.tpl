<div class="col-md-12 content_bg_color">
    <div id="bread" class="form-group">
        <ol class="breadcrumb">
            <li><a href="{$baseurl}/account">My account</a></li>
            <li class="active">Change E-Mail</li>
        </ol>
    </div>

    <div class="col-md-12">
        <h2>Change E-Mail</h2>

        <!-- change email start -->
        <form action="" method="post">
            <!-- current email start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input disabled type="text" value="{if $sNewEmail}{$sNewEmail}{else}{$oUser->Email}{/if}" class="form-control">
                </div>
            </div>
            <!-- current email end -->

            <!-- new email start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" placeholder="New E-Mail" value="" name="change_email" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default">Change E-Mail</button>
                <input type="hidden" name="send" value="change_email"/>

                <div style="padding-top:10px;">
                    {if $error}
                        <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
                    {/if}
                    {if $success}
                        <div class="alert alert-success"><i class="glyphicon glyphicon-check"></i> {$success}</div>
                    {/if}
                </div>
            </div>
            <!-- new email end -->
        </form>
        <!-- change email end -->
    </div>
</div>