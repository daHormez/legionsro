<div class="col-md-12 content_bg_color">
    <div id="bread" class="form-group">
        <ol class="breadcrumb">
            <li class="active">My account</li>
        </ol>
    </div>

    <div class="col-md-12 col-reset-padding">
        <div class="col-md-6">
            <div class="col-md-12 col-reset-padding-left">
                <h2>My account</h2>
            </div>

            <!-- password start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- change password start -->
                <a href="{$baseurl}/account/changepw" class="btn btn-default btn-width-200">Change password</a>
                <!-- change password end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- password end -->

            <!-- email start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- change email start -->
                <a href="{$baseurl}/account/changeemail" class="btn btn-default btn-width-200">Change E-Mail</a>
                <!-- change email end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- email end -->

            <!-- buy silk start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- buy silk start -->
                <a href="{$baseurl}/account/buysilk" class="btn btn-default btn-width-200">Buy Silk</a>
                <!-- buy silk end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- buy silk end -->

            <!-- reset char start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- reset character start -->
                <a href="{$baseurl}/account/resetchar" class="btn btn-default btn-width-200">Reset Character</a>
                <!-- reset character end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- reset char end -->

            <!-- vote 4 silk start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- reset character start -->
                <a href="#" disabled class="btn btn-default btn-width-200"><span class="label label-danger">Coming soon (Vote for Silk)</span></a>
                <!-- reset character end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- vote 4 silk end -->

            <!-- lottery start -->
            <div class="col-md-12 col-reset-padding-left">
                <!-- reset character start -->
                <a href="#" disabled class="btn btn-default btn-width-200"><span class="label label-danger">Coming soon (Lottery)</span></a>
                <!-- reset character end -->
                <div style="padding-bottom: 10px;"></div>
            </div>
            <!-- lottery end -->
        </div>

        <!-- infomation start -->
        <div class="col-md-6">
            <div class="col-md-12 col-reset-padding-left">
                <h2>Information</h2>
            </div>

            <div class="col-md-12 col-reset-padding-left">
                <div class="form-group">
                    <label class="label label-default" for="silk">Current Silk</label>
                    {if !$oSilk->silk_own}
                        {assign var=sSilkAmount value="0"}
                    {else}
                        {assign var=sSilkAmount value=$oSilk->silk_own}
                    {/if}
                    <input class="form-control" type="text" disabled id="silk" value="{$sSilkAmount}">
                </div>

                <div class="form-group">
                    <label class="label label-default" for="gold">Current Gold <span>(All Character)</span></label>

                    {assign var=sGold value=0}
                    {foreach from=$aInfos key=k item=i}
                        {assign var=sGold value=$sGold+$i->RemainGold}
                    {/foreach}

                    <input class="form-control" type="text" disabled id="gold" value="{number_format($sGold)}">
                </div>
            </div>
            <div style="padding-bottom: 10px;"></div>
        </div>
    </div>
    <!-- infomation end -->
</div>