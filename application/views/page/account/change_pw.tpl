<div class="col-md-12 content_bg_color">
    <div id="bread" class="form-group">
        <ol class="breadcrumb">
            <li><a href="{$baseurl}/account">My account</a></li>
            <li class="active">Change password</li>
        </ol>
    </div>

    <div class="col-md-12">
        <h2>Change password</h2>

        <!-- change password start -->
        <form action="" method="post">
            <!-- password start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" placeholder="New password" name="change_password" class="form-control" id="password">
                </div>
            </div>
            <!-- password end -->

            <!-- password repeat start -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" placeholder="Repeat new password" name="change_password_repeat" class="form-control" id="password_repeat">
                </div>
            </div>
            <!-- password repeat end -->

            <div class="form-group">
                <button type="submit" class="btn btn-default">Change password</button>
                <input type="hidden" name="send" value="change_password"/>

                <div style="padding-top:10px;">
                    {if $error}
                        <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
                    {/if}
                    {if $success}
                        <div class="alert alert-success"><i class="glyphicon glyphicon-check"></i> {$success}</div>
                    {/if}
                </div>
            </div>
            <!-- new email end -->
        </form>
        <!-- change password end -->
    </div>
</div>