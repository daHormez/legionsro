<div class="col-md-12 content_bg_color">
    <div id="bread" class="form-group">
        <ol class="breadcrumb">
            <li><a href="{$baseurl}/account">My account</a></li>
            <li class="active">Buy silk</li>
        </ol>
    </div>

    <div class="col-md-12">
        <h3>Buy silk</h3>

        <div class="form-group">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="48F5BEUK6D5LQ">
                <input type="hidden" name="custom" value="{$oUser->StrUserID}">
                <table>
                    <tr><td><input type="hidden" name="on0" value="Silks">Silks</td></tr><tr><td><select class="form-control" name="os0">
                                <option value="250 Silks">250 Silks $3,00 USD</option>
                                <option value="500 Silks">500 Silks $6,00 USD</option>
                                <option value="750 Silks + 50 Bonus">750 Silks + 50 Bonus $9,00 USD</option>
                                <option value="1000 Silks + 100 Bonus">1000 Silks + 100 Bonus $12,00 USD</option>
                                <option value="1250 Silks + 150 Bonus">1250 Silks + 150 Bonus $15,00 USD</option>
                                <option value="2500 Silks">2500 Silks $25,00 USD</option>
                            </select> </td></tr>
                </table>
                <input type="hidden" name="return" value="{$baseurl}/account/buysilk/success">
                <input type="hidden" name="notify_url" value="{$baseurl}/account/buysilk/notify">
                <input type="hidden" name="currency_code" value="USD">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/pt_PT/i/scr/pixel.gif" width="1" height="1">
            </form>
        </div>

        <div class="form-group">
            {if $error}
                <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
            {/if}

            {if $success}
                <div class="alert alert-success"><i class="glyphicon glyphicon-check"></i> {$success}</div>
            {/if}
        </div>
    </div>
</div>

<!--
<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{$sPaypalEmail}">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="item_name" value="Online Goods">
    <input type="hidden" name="custom" value="{$userid}">

    <p>
        Choose the Amount:
    </p>

    <select class="form-control" name='amount'>
        <option value="250 Silk">250 Silk $3.00 USD</option>
        <option value="500 Silk">500 Silk $6.00 USD</option>
        <option value="750 Silk + 50 Bonus">750 Silk + 50 Bonus $9.00 USD</option>
        <option value="1050 Silk + 75 Bonus">1050 Silk + 75 Bonus $12.00 USD</option>
        <option value="1250 Silk + 100 Bonus">1250 Silk + 100 Bonus $15.00 USD</option>
        <option value="2500 Silk">2500 Silk $25.00 USD</option>
    </select>
    <input type="hidden" name="return" value="{$baseurl}/account/buysilk/success">
    <input type="hidden" name="notify_url" value="{$baseurl}/account/buysilk/notify">
    <br>
    <input type="image" src="http://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" />
</form>
-->