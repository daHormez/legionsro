<div class="col-md-12 content_bg_color">
    <div id="bread" class="form-group">
        <ol class="breadcrumb">
            <li><a href="{$baseurl}/account">My account</a></li>
            <li class="active">Reset character</li>
        </ol>
    </div>

    <div class="col-md-12">
        <h2>Reset Character</h2>

        <p>Reset your characters stat points by following chars</p>

        <!-- change email start -->
        <form action="" method="post">
            <div class="row">
                {if !$aInfos}
                    <div class="col-md-12">
                        <p class="text-danger">There are no character to reset!</p>
                    </div>
                {else}
                    {foreach from=$aInfos key=k item=i}
                        <div class="col-sm-6 col-md-4 reset_char_box">
                            <div class="col-md-12">
                                <h3>{$i->CharName16}</h3>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Level</p>
                                <p>Hp</p>
                                <p>Mp</p>
                                <p>Gold</p>
                                <p>Strength</p>
                                <p>Intellect</p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>{$i->CurLevel}</p>
                                <p><span class="text-danger">{$i->HP}</span></p>
                                <p><span class="text-info">{$i->MP}</span></p>
                                <p><span>{$i->RemainGold}</span></p>
                                <p><span>{$i->Strength}</span></p>
                                <p><span>{$i->Intellect}</span></p>
                            </div>
                            <div class="col-md-12">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger">Reset</button>
                                        <input type="hidden" name="char" value="{$i->CharID}">
                                        <input type="hidden" name="send" value="reset"/>

                                        <div style="padding-top:10px;">
                                            {if $success_{$i->CharID}}
                                                <div class="alert alert-success"><i class="glyphicon glyphicon-check"></i> {$success_{$i->CharID}}</div>
                                            {/if}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </form>
        <!-- change email end -->
    </div>
</div>