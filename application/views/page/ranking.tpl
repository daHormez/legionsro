<div class="col-md-12 content_bg_color">
    <h2>Ranking</h2>

    <p>Total Accounts: <b>{$sTotalAccountCount}</b> | Total Chars: <b>{$sTotalCharCount}</b></p>

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#all" role="tab" data-toggle="tab">
                    Player ranking
                </a>
            </li>
            <li role="presentation">
                <a href="#trader" role="tab" data-toggle="tab">
                    Trader ranking
                </a>
            </li>
            <li role="presentation">
                <a href="#robber" role="tab" data-toggle="tab">
                    Thief ranking
                </a>
            </li>
            <li role="presentation">
                <a href="#hunter" role="tab" data-toggle="tab">
                    Hunter ranking
                </a>
            </li>
            <li role="presentation">
                <a href="#guilds" role="tab" data-toggle="tab">
                    Guild ranking
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all">
                <table class="table table-hover">
                    <th>Char name</th>
                    <th>Level</th>
                    <th>Guild</th>
                    {foreach from=$oChar->getRankingChars() key=k item=i name=all}
                        {if $i->Name == "dummy"}
                            {assign var=sGuildName value="<span style='color: red;'>No Guild</span>"}
                        {else}
                            {assign var=sGuildName value=$i->GuildName}
                        {/if}

                        <!-- top player -->
                        {if $smarty.foreach.all.first}
                            <tr class="info">
                                <td><span class="label label-danger">Best</span> {$i->CharName16}</td>
                                <td>{$i->CurLevel}</td>
                                <td>{$sGuildName}</td>
                            </tr>
                        {else}
                            <tr>
                                <td>{$i->CharName16}</td>
                                <td>{$i->CurLevel}</td>
                                <td>{$sGuildName}</td>
                            </tr>
                        {/if}
                    {/foreach}
                </table>
            </div>

            <!-- trader ranking start -->
            <div role="tabpanel" class="tab-pane" id="trader">
                {if empty($oTrader)}
                    <div class="container">
                        <p class="text-danger">No Trader data</p>
                    </div>
                {else}
                    <table class="table table-hover">
                        <th>Char name</th>
                        <th>Level</th>
                        <th>Experience</th>
                        {foreach from=$oTrader key=k item=i name=allTrader}
                            <!-- top trader -->
                            {if $smarty.foreach.allTrader.first}
                                <tr class="info">
                                    <td><span class="label label-danger">Best</span> {$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {else}
                                <tr>
                                    <td>{$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {/if}
                        {/foreach}
                    </table>
                {/if}
            </div>
            <!-- trader ranking end -->

            <!-- robber ranking start -->
            <div role="tabpanel" class="tab-pane" id="robber">
                {if empty($oRobber)}
                    <div class="container">
                        <p class="text-danger">No Robber data</p>
                    </div>
                {else}
                    <table class="table table-hover">
                        <th>Char name</th>
                        <th>Level</th>
                        <th>Experience</th>
                        {foreach from=$oRobber key=k item=i name=allRobber}
                            <!-- top robber -->
                            {if $smarty.foreach.allRobber.first}
                                <tr class="info">
                                    <td><span class="label label-danger">Best</span> {$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {else}
                                <tr>
                                    <td>{$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {/if}
                        {/foreach}
                    </table>
                {/if}
            </div>
            <!-- robber ranking end -->

            <!-- hunter ranking start -->
            <div role="tabpanel" class="tab-pane" id="hunter">
                {if empty($oHunter)}
                    <div class="container">
                        <p class="text-danger">No Hunter data</p>
                    </div>
                {else}
                    <table class="table table-hover">
                        <th>Char name</th>
                        <th>Level</th>
                        <th>Experience</th>
                        {foreach from=$oHunter key=k item=i name=allHunter}
                            <!-- top hunter -->
                            {if $smarty.foreach.allHunter.first}
                                <tr class="info">
                                    <td><span class="label label-danger">Best</span> {$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {else}
                                <tr>
                                    <td>{$oChar->getCharById($i->CharID)->CharName16}</td>
                                    <td>{$i->Level}</td>
                                    <td>{$i->Exp}</td>
                                </tr>
                            {/if}
                        {/foreach}
                    </table>
                {/if}
            </div>
            <!-- hunter ranking end -->

            <!-- guild ranking start -->
            <div role="tabpanel" class="tab-pane" id="guilds">
                {if empty($oGuild)}
                    <div class="container">
                        <p class="text-danger">No Guild data</p>
                    </div>
                {else}
                    <table class="table table-hover">
                        <th>Guild name</th>
                        <th>Level</th>
                        <th>Member</th>
                        <th>Gathered sp</th>
                        {nocache}
                            {foreach from=$oGuild key=k item=i name=allGuilds}
                                <!-- top guild -->
                                {if $smarty.foreach.allGuilds.first}
                                    <tr class="info">
                                        <td><span class="label label-danger">Best</span> {$i->Name}</td>
                                        <td>{$i->Lvl}</td>
                                        <td>{$i->MemberCount}</td>
                                        <td>{$i->GatheredSP}</td>
                                    </tr>
                                {else}
                                    <tr>
                                        <td>{$i->Name}</td>
                                        <td>{$i->Lvl}</td>
                                        <td>{$i->MemberCount}</td>
                                        <td>{$i->GatheredSP}</td>
                                    </tr>
                                {/if}
                            {/foreach}
                        {/nocache}
                    </table>
                {/if}
            </div>
            <!-- guild ranking end -->
        </div>
    </div>
</div>