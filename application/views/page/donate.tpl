<div class="col-md-12 content_bg_color">
    <h3>Donate</h3>

    <div class="form-group">
        <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="{$sPaypalEmail}">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="item_name" value="donation">

            <p style="text-align: center; color: red;">If you donate, <b>YOU ARE NOT</b> purchasing silk, <b>you won't</b> receive any silk. This donation is to help maintain the server and can also be as less then 1$</p>
            
            <p>Choose the Amount:</p>

            <select class="form-control" name='amount'>
                <option value='0.1'>USD 0.10</option>
                <option value='0.5'>USD 0.50</option>
                <option value='1'>USD 1.00</option>
                <option value='1'>USD 2.00</option>
                <option value='5'>USD 5.00</option>
                <option value='10'>USD 10.00</option>
                <option value='15'>USD 15.00</option>
                <option value='20'>USD 20.00</option>
                <option value='25'>USD 25.00</option>
                <option value='30'>USD 30.00</option>
                <option value='50'>USD 50.00</option>
            </select>
            <input type="hidden" name="return" value="{$baseurl}/donate/success">
            <br>
            <input type="image" src="http://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" />
        </form>
    </div>
</div>