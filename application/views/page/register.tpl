<div class="col-md-12 content_bg_color">
    <h2>Register</h2>

    <form role="form" method="post" action="">
        <!-- username start -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" placeholder="Username" value="{$aPost.reg_username}" name="reg_username" class="form-control" id="username">
            </div>
        </div>
        <!-- username end -->

        <!-- email start -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input type="text" placeholder="E-Mail" value="{$aPost.reg_email}" name="reg_email" class="form-control" id="email">
            </div>
        </div>
        <!-- email end -->

        <!-- password start -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" placeholder="Password" name="reg_password" class="form-control" id="password">
            </div>
        </div>
        <!-- password end -->

        <!-- password repeat start -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" placeholder="Repeat password" name="reg_password_repeat" class="form-control" id="password_repeat">
            </div>
        </div>
        <!-- password repeat end -->

        <div class="form-group">
            <input type="checkbox" name="terms" id="terms">
            <label for="terms">Accept Terms of uses and privacy Policy</label>
        </div>

        <button type="submit" class="btn btn-default">Register</button>
        <input type="hidden" name="send" value="register"/>

        <div class="form-group">
            {if $error}
                <div class="alert alert-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> {$error}</div>
            {/if}
        </div>
    </form>
</div>