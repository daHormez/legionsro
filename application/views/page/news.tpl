<div class="col-md-12 content_bg_color">
    <div style="padding-top: 10px;" class="col-md-9 col-reset-padding-left">
        {if empty($aNews)}
            <div style="padding-bottom: 10px;" class="col-md-6">
                <span class="glyphicon glyphicon-exclamation-sign"></span> <span class="text-danger">No news at the moment</span>
            </div>
        {else}
            {foreach from=array_reverse($aNews) key=k item=i name=news}
                    <div class="accordion-group">
                        <div class="head_comment">
                            <div class="accordion-heading">
                                <h4 style="margin-top: 5px; !important;">
                                    <span style="float: left;">
                                        {if $i->TYPE == "Notice"}
                                            <span class="label label-info">{$i->TYPE}</span>
                                        {elseif $i->TYPE == "Announcement"}
                                            <span class="label label-warning">{$i->TYPE}</span>
                                        {elseif $i->TYPE == "Update"}
                                            <span class="label label-danger">{$i->TYPE}</span>
                                        {/if}
                                    </span>
                                    {ucfirst($i->HEADING)}
                                </h4>
                            </div>
                        </div>

                        <div class="body_comment">
                            <div id="collapse_{$smarty.foreach.news.iteration}" class="accordion-body {if $smarty.foreach.news.first}collapse in{else}collapse{/if}">
                                <div class="accordion-inner">
                                    <p>{$i->TEXT}</p>
                                </div>
                            </div>
                            <button class="btn btn-default btn-width-200 newsButton" data-toggle="collapse" {if $smarty.foreach.news.first}aria-expanded="true"{/if} href="#collapse_{$smarty.foreach.news.iteration}"></button>
                            <span style="line-height: 32px; float: right;"><i class="glyphicon glyphicon-calendar"></i> {$i->CREATE_DATE|date_format:"%m.%d.%Y"}</span>
                        </div>
                    </div>
            {/foreach}
        {/if}
    </div>
    <div class="col-md-3 col-reset-padding">
        <div class="container" style="padding-bottom: 10px;">
            <h2>Fortress</h2>

            {if !$sHotanGuild}
                {assign var=sHotanGuild value="None"}
            {/if}
            {if !$sJanganGuild}
                {assign var=sJanganGuild value="None"}
            {/if}
            {if !$sBanditGuild}
                {assign var=sBanditGuild value="None"}
            {/if}

            <table>
                <tr>
                    <td width="30"><span class="fortress_hotan_icon"></span></td>
                    <td width="65"><span>Hotan:</span></td>
                    <td><span>{$sHotanGuild}</span></td>
                </tr>
                <tr>
                    <td width="30"><span class="fortress_jangan_icon"></span></td>
                    <td width="65"><span>Jangan:</span></td>
                    <td><span>{$sJanganGuild}</span></td>
                </tr>
                <tr>
                    <td width="30"><span class="fortress_bandit_icon"></span></td>
                    <td width="65"><span>Bandit:</span></td>
                    <td><span>{$sBanditGuild}</span></td>
                </tr>
            </table>

            {*<span class="fortress_const_icon">{$sConstGuild}</span>*}
        </div>

        <hr style="border-color: #666666;">

        <div class="container">
            <h2>Server Stats</h2>

            <p>Exp: {$sStatExp}x</p>
            <p>Sp: {$sStatSp}x</p>
            <p>Gold: {$sStatGold}x</p>
            <p>Item drop: {$sStatItem}x</p>
            <p>Sox drop: {$sStatItemSox}x</p>
        </div>

        <div class="container">
            <h2>Partner</h2>

            <p>
                <a href="http://elitepvpers.com/"><img width="250" src="out/pictures/epvp.png"></a>
            </p>

            <p>
                <a href="www.elitepvpers.com/forum/silkroad-online-trading/3489387-torque-exploit-protection-proxy-vsro.html"><img width="250" src="out/pictures/torque.png"></a>
            </p>
        </div>
    </div>
</div>
