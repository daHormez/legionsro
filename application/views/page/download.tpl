<div class="col-md-12 content_bg_color">
    <h2>Downloads</h2>

    <p style="padding-bottom: 10px;">
        {if $dlone != null && $dlonelink != null}
            <a target="_blank" href="{$dlonelink}" class="btn btn-info btn-width-200"><span class="glyphicon glyphicon-arrow-down"></span> {$dlone}</a>
        {/if}

        {if $dltwo != null && $dltwolink != null}
            <a target="_blank" href="{$dltwolink}" class="btn btn-info btn-width-200"><span class="glyphicon glyphicon-arrow-down"></span> {$dltwo}</a>
        {/if}

        {if $dlthree != null && $dlthreelink != null}
            <a target="_blank" href="{$dlthreelink}" class="btn btn-info btn-width-200"><span class="glyphicon glyphicon-arrow-down"></span> {$dlthree}</a>
        {/if}

        {if $dlfour != null && $dlfourlink != null}
            <a target="_blank" href="{$dlfourlink}" class="btn btn-info btn-width-200"><span class="glyphicon glyphicon-arrow-down"></span> {$dlfour}</a>
        {/if}

        {if $dlonelink == null && $dltwolink == null && $dlthreelink == null && $dlfourlink == null}
            <p class="text-danger">No Downloads available</p>
        {/if}
    </p>
</div>