<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class LogHelper
{
    private $_sTmpPath = "tmp/";
    private $_sLogFile = "log.txt";

    /**
     * Logs the Controller
     *
     * @param string $sController a controller
     * @param string $sAction     a method
     */
    public function log($sController = '', $sAction = '')
    {
        if (empty($sAction) || $sAction == "indexAction") {
            $aLogContent[] = PHP_EOL . "Es wurde versucht " . $sController . " aufzurufen" . PHP_EOL;
        } else {
            $aLogContent[] = PHP_EOL . "Es wurde versucht " . $sAction . " in " . $sController . " aufzurufen" . PHP_EOL;
        }

        $aLogContent[] = "- " . date('Y-m-d H:i:s'); // just the date

        $sContent = implode(" ", $aLogContent);

        $this->updateLogFile($sContent, FILE_APPEND);
    }

    /**
     * Updates the log-file
     *
     * @param string    $sContent    logfile content
     * @param reccource $oFileAppend apped data
     *
     * @throws Exception
     */
    private function updateLogFile($sContent, $oFileAppend = null)
    {
        $blUpdate = file_put_contents($this->_sTmpPath . $this->_sLogFile, $sContent, $oFileAppend);

        if (!$blUpdate) {
            throw new Exception("Log Eintrag fehlgeschlagen!");
        }
    }

    /**
     * cleans the log
     *
     * @todo log öfter cleanen :D (Wieso nicht darauf programmiert? Weil ich mir nur Fehlermeldungen loggen wollte! Ole Ole)
     *
     * @param integer $iDays anzahl von tagen
     */
    public function cleanLog($iDays)
    {
        $sTime = time();
        if ($sTime - filemtime($this->_sTmpPath . $this->_sLogFile)
            >= 60 * 60 * 24 * $iDays
        ) {
            $this->updateLogFile(null);
        }
    }
}
