<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */
class ERROR_PAGE_404
{

    private $_sTitle = null;
    private $_sMessage = null;
    private $_iCode = null;

    /**
     * basic error construct :D
     *
     * @param string $sErrorTitle   a error title
     * @param string $sErrorMessage a error message
     * @param int    $iErrorCode    a error code
     */
    public function __construct($sErrorTitle = "", $sErrorMessage = "", $iErrorCode = 0)
    {
        $this->_sTitle = $sErrorTitle;
        $this->_sMessage = $sErrorMessage;
        $this->_iCode = $iErrorCode;

        $oConfig = ConfigController::getInstance();

        $this->buildErrorPage($oConfig->getVar("baseurl"));
    }

    /**
     * Gets the error title for our 404-Page.
     *
     * @return string
     */
    private function getTitle()
    {
        return $this->_sTitle;
    }

    /**
     * Gets the error message for our 404-Page.
     *
     * @return string
     */
    private function getMessage()
    {
        return $this->_sMessage;
    }

    /**
     * Gets the error code for our 404-Page.
     *
     * @return string
     */
    private function getCode()
    {
        return $this->_iCode;
    }

    /**
     * Builds a error page
     *
     * @param sting $sBaseUrl base url
     */
    private function buildErrorPage($sBaseUrl)
    {
        echo '<!DOCTYPE html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo "<link href='" . $sBaseUrl . "/out/src/css/bootstrap.min.css' rel='stylesheet'>";
        echo '<!--[if lt IE 9]>';
        echo '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>';
        echo '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>';
        echo '<![endif]-->';
        echo '</head>';
        echo '<body>';
        echo '<div class="container">';
        echo '<div class="alert alert-danger" role="alert">';
        echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
        echo '<span class="sr-only">Error:</span>';
        echo " " . $this->getTitle() . " " . $this->getCode() . ": " . $this->getMessage();
        echo '</div>';
        echo '</div>';
        echo '</body>';
        echo '</html>';
    }
}
