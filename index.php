<?php

/**
 * Created by BaboTools
 *
 * @author    Daniel Hormess <hormess@solutionDrive.de>
 * @date      23.01.15
 * @time      10:00
 * @copyright 2015 BaboTools
 */

error_reporting(E_WARNING);
setlocale(LC_TIME, "de_DE");
/**
 * @param string $sClassname a classname
 *
 * @throws ClassfileException
 */
function myAutoloader($sClassname)
{
    include_once "application/exceptions/ClassfileException.php";

    // Namensteile der Klassen und zugehoerige Verzeichnisse
    $aClasstype2directory = array(
        'Controller' => array('application/controllers/', 'application/controllers/system/'),
        'Helper'     => array('application/helpers/'),
        'Exception'  => array('application/exceptions/'),
        'Model'      => array('application/models/', 'application/models/system/'),
    );

    // Klassendatei laden anhand des Klassennamens
    foreach ($aClasstype2directory as $sClasstype => $aDirs) {
        $blFileFound = false;
        $blClassNameIdentified = false;

        if (substr($sClassname, strlen($sClassname) - strlen($sClasstype)) == $sClasstype) {
            $blClassNameIdentified = true;

            foreach ($aDirs as $sDir) {
                $sClasspath = $sDir . $sClassname . '.php';

                if (file_exists($sClasspath)) {
                    include_once $sClasspath;
                    $blFileFound = true;
                    break;
                }
            }
        }

        if ($blFileFound) {
            break;
        }
    }

    if ($blClassNameIdentified) {
        // wenn file wurde nicht gefunden wurde
        if (!$blFileFound) {
            throw new ClassfileException("Datei fuer Klasse '{$sClassname}' wurde nicht gefunden.");
        }
        // wenn klasse nicht gefunden wurde
        if (!class_exists($sClassname)) {
            throw new ClassfileException("Klassendefinition fuer '{$sClassname}' wurde nicht gefunden.");
        }
    }
}

spl_autoload_register('myAutoloader');

// Controller, die nicht ueber die URL direkt aufgerufen werden duerfen                          
$aNoDirectCallsControllers = array(
    'BaseController',
    'ViewController',
    'SessionController',
    'ConfigController',
    'DatabaseAccountController',
    'DatabaseShardController',
    'EmailController',
);

$aUpdateControllers = array(

);

try {
    // URL auswerten
    list($sController, $sAction, $aParams) = BaseController::analyzeUrl($_GET['q']); // URL wurde durch htaccess als index.php?q= angehÃ¤ngt

    if (in_array($sController, $aNoDirectCallsControllers) && $sAction != "updateAction" && count($aParams) == 0) {
        if (in_array($sController, $aUpdateControllers)) {
            throw new Exception("Klasse '{$sController}' kann nicht direkt aufgerufen werden. Jedoch kann eine Update Funktion ausgeführt werden");
        }

        throw new Exception("Klasse '{$sController}' kann nicht direkt aufgerufen werden.");
    }

    if (!class_exists($sController)) {
        throw new Exception("Klasse '{$sController}' wurde nicht gefunden.", 404);
    }

    $oController = new $sController; // Instanziierung des Controllers

    if (!method_exists($oController, $sAction)) {
        throw new Exception("Action '$sAction' in der Klasse '{$sController}' wurde nicht gefunden.", 404);
    }

    $oController->$sAction($aParams); // Aktion ausfÃ¼hren
} catch (Exception $oEx) {
    // Error page
    include_once '404.php';
    new ERROR_PAGE_404("Error ", $oEx->getMessage(), $oEx->getCode());

    // logger
    $oLog = new LogHelper();
    $oLog->cleanLog(1);                      // just deletes log file after 1 day
    $oLog->log($sController, $sAction);      // logs controller and action errors

    die();
}
